<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
  if(localStorage.getItem('token')==null)
      window.location="{{url('/admin')}}";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dashboard - Admin Template</title>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/theme.css')}}" />
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/style.css')}}" />

</head>

<body>  
<div id="container">
      <div id="header">
          <h2>Makemymeals admin panel</h2>
           <div id="topmenu">
              <ul>
                  <li><a href="{{url('/admin/home')}}" >Dashboard</a></li>
                    <li><a href="{{url('/admin/orders')}}" id="orders">Orders</a></li>
                  <li><a href="{{url('/admin/users')}}">Users</a></li>
                    <li class="current"><a href="{{url('/admin/foods')}}">Foods</a></li>
                    
              </ul>
          </div>
          <button id="logout" style="padding:3px 8px;background-color:#1ab188;color:white;float:right" >LOGOUT</button>

      </div>
        <div id="error">
            
         </div
  </div>
  <div id="sidebar">
    <ul>
      
      <li><h3><a href="{{url('/admin/home')}}" class="house">Dashboard</a></h3>
          <ul>
              <li><a href="{{url('/admin/home')}}" class="report">Show Tiffins on DATE</a></li>
              <!-- <li><a href="#" class="report_seo">A</a></li> -->
              
          </ul>
      </li>
      <li><h3><a href="{{url('/admin/orders')}}" class="folder_table">Orders</a></h3>
            <ul>
              <li><a href="{{url('/admin/order/create')}}" class="addorder">Create Order</a></li>
            <li><a href="{{url('/admin/orders')}}" class="shipping">Show Orders</a></li>
              
          </ul>
      </li>
      
    <li><h3><a href="{{url('/admin/users')}}" class="user">Users</a></h3>
            <ul>
              <li><a href="{{url('/admin/user/create')}}" class="useradd">Create user</a></li>
              <li><a href="{{url('/admin/users')}}" class="group">Show Users</a></li>
              
          </ul>
      </li>

      <li><h3><a href="{{url('/admin/foods')}}" class="manage">Foods</a></h3>
            <ul>
              <li><a href="{{url('/admin/food/create')}}" class="manage_page">Create Food</a></li>
              <li><a href="{{url('/admin/foods')}}" class="cart">Show Foods</a></li>
              <!-- <li><a href="#" class="folder">Product categories</a></li>
              <li><a href="#" class="promotions">Promotions</a></li> -->
          </ul>
      </li>
  </ul>       
</div>
	

		<div id="wrapper">
            <div id="content">
                <div id="box" style="text-align:center;width:800px;margin-left:-50px;">
                	<h3>Foods</h3>
                  <div id="show">
                  </div>
                	<table width="100%" id="table">
						<thead>
							<tr>
              	<th width="40px"><a href="#">Food ID</a></th>
              	<th width="100px"><a href="#" >Name</a></th>
                  <th width="10px"><a href="#">L</a></th>
                  <th width="10px"><a href="#">D</a></th>
              </tr>
						</thead>
						<tbody id="foods">
							
						</tbody>
					</table>

            <script src="{{URL::asset('js/jquery.js')}}"></script>

  <script>	
    $(document).ready((function(){
            var $o = $('#foods');
            var token=localStorage.getItem('token');
            var order={
                token:token ,
              }

            $.ajax({
                type: 'POST',
                url: '../api/food',
                dataType: 'json',
                data: order,
                success: function(elm){
       
                $.each(elm.results.food, function(){
                    
                $o.append('<tr><td><a href="foods/'+this.foodid+'">'+this.foodid+'</a></td><td>'+this.name+'</td><td>'+this.L+'</td><td>'+this.D+'</td></tr>');
           
                 });

                function pageButtons(t,e){var $=1==e?"disabled":"",n=e==t?"disabled":"",i="<input type='button' class='prev' value='&lt;&lt; Prev'"+$+">";for($i=1;t>=$i;$i++)i+="<input type='button' class='pageno' id='id"+$i+"'value='"+$i+"'>";return i+="<input type='button' class='next' value='Next &gt;&gt;'"+n+">"}var $table=document.getElementById("table"),$n=12,$rowCount=$table.rows.length,$firstRow=$table.rows[0].firstElementChild.tagName,$hasHead="TH"===$firstRow,$tr=[],$i,$ii,$j=$hasHead?1:0,$th=$hasHead?$table.rows[0].outerHTML:"",$pageCount=Math.ceil(($rowCount-1)/$n);if($pageCount>1){for($i=$j,$ii=0;$rowCount>$i;$i++,$ii++)$tr[$ii]=$table.rows[$i].outerHTML;$table.insertAdjacentHTML("afterend","<div id='buttons'></div"),$p=1;var $rows=$th,$s=$n*$p-$n;for($i=$s;$s+$n>$i&&$i<$tr.length;$i++)$rows+=$tr[$i];$table.innerHTML=$rows,document.getElementById("buttons").innerHTML=pageButtons($pageCount,$p),document.getElementById("id"+$p).setAttribute("class","active")}$(document).on("click",".pageno",function(){$p=$(this).val();var t=$th,e=$n*$p-$n;for($i=e;e+$n>$i&&$i<$tr.length;$i++)t+=$tr[$i];$table.innerHTML=t,document.getElementById("buttons").innerHTML=pageButtons($pageCount,$p),document.getElementById("id"+$p).setAttribute("class","active")}),$(document).on("click",".prev",function(){$p=$(".active").val()-1;var t=$th,e=$n*$p-$n;for($i=e;e+$n>$i&&$i<$tr.length;$i++)t+=$tr[$i];$table.innerHTML=t,document.getElementById("buttons").innerHTML=pageButtons($pageCount,$p),document.getElementById("id"+$p).setAttribute("class","active")}),$(document).on("click",".next",function(){$p=$(".active").val(),$p++;var t=$th,e=$n*$p-$n;for($i=e;e+$n>$i&&$i<$tr.length;$i++)t+=$tr[$i];$table.innerHTML=t,document.getElementById("buttons").innerHTML=pageButtons($pageCount,$p),document.getElementById("id"+$p).setAttribute("class","active")});
                }
            });

            $('#logout').on('click', function(){

              var token=localStorage.getItem('token');
              var order={
                  token:token,
              };
              $.ajax({
                  type: 'POST',
                  url: '../api/adminlogout',
                  data: order,
                  success: function(elm){
                      if(elm.results.success){
                        localStorage.removeItem('token');
                        window.location="{{url('/admin')}}";
                      }
                      else{
                        alert('Internal server Error');
                      }
                  }
          });
              
      });


    }));
  </script>
            
    
<a href="food/create"><h3>Create food</h3></a>
        