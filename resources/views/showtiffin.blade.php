<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
  if(localStorage.getItem('token')==null)
      window.location="{{url('/admin')}}";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dashboard - Admin Template</title>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/theme.css')}}" />
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/style.css')}}" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="{{URL::asset('js/jqueryui.js')}}"></script>
</head>

<body>  
<div id="container">
      <div id="header">
          <h2>Makemymeals admin panel</h2>
           <div id="topmenu">
              <ul>
                  <li ><a href="{{url('/admin/home')}}" >Dashboard</a></li>
                    <li class="current"><a href="{{url('/admin/orders')}}" id="orders">Orders</a></li>
                  <li><a href="{{url('/admin/users')}}">Users</a></li>
                    <li><a href="{{url('/admin/foods')}}">Foods</a></li>
                    
              </ul>
          </div>
          <button id="logout" style="padding:3px 8px;background-color:#1ab188;color:white;float:right" >LOGOUT</button>

      </div>
        <div id="error">
            
         </div
  </div>
  <div id="sidebar">
    <ul>
      
      <li><h3><a href="{{url('/admin/home')}}" class="house">Dashboard</a></h3>
          <ul>
              <li><a href="{{url('/admin/home')}}" class="report">Show Tiffins on DATE</a></li>
              <!-- <li><a href="#" class="report_seo">A</a></li> -->
              
          </ul>
      </li>
      <li><h3><a href="{{url('/admin/orders')}}" class="folder_table">Orders</a></h3>
            <ul>
              <li><a href="{{url('/admin/order/create')}}" class="addorder">Create Order</a></li>
            <li><a href="{{url('/admin/orders')}}" class="shipping">Show Orders</a></li>
              
          </ul>
      </li>
      
    <li><h3><a href="{{url('/admin/users')}}" class="user">Users</a></h3>
            <ul>
              <li><a href="{{url('/admin/user/create')}}" class="useradd">Create user</a></li>
              <li><a href="{{url('/admin/users')}}" class="group">Show Users</a></li>
              
          </ul>
      </li>

      <li><h3><a href="{{url('/admin/foods')}}" class="manage">Foods</a></h3>
            <ul>
              <li><a href="{{url('/admin/food/create')}}" class="manage_page">Create Food</a></li>
              <li><a href="{{url('/admin/foods')}}" class="cart">Show Foods</a></li>
              <!-- <li><a href="#" class="folder">Product categories</a></li>
              <li><a href="#" class="promotions">Promotions</a></li> -->
          </ul>
      </li>
  </ul>       
</div>
  

    <div id="wrapper">
            <div id="content">
                <div id="box">
                  <h3>Tiffins</h3>
                  <div id="show"></div>
                    <form id="myform" >
                    <table id="table" style="text-align:center;">
                         
                    </table>
                    <input type="submit" value="Update Tiffin"/>
                    </form>

           </div>
        </div>
    </div>
</body>
  <script >  
                    $(document).ready((function(){
                    var $o = $('#table');
                    var a={{$tiffinid}};
                    var token=localStorage.getItem('token');
                    var order={
                        token:token ,
                      }
                    
                    $.ajax({
                    type: 'POST',
                    url: '../../api/showtiffins',
                    dataType: 'json',
                    data: order,
                    success: function(elm){
                        
                    $.each(elm.results.tiffins, function(){
                        
                        if(this.tiffinid==a){
                        $o.append('<tr><th>'+"Tiffin ID"+'</th><td><input type="text" disabled="true"  id="tiffinid" value="'+this.tiffinid+'"/></td></tr><tr><th>'
                                            +"L"+'</th><td><input type="text" id="L" disabled="true"  value="'+this.L+'"/></td></tr><tr><th>'
                                            +"D"+'</th><td><input type="text" id="D" disabled="true"  value="'+this.D+'"/></td></tr><tr><th>'
                                            +"Booking ID"+'</th><td><input type="text" disabled="true"  id="bookingid" value="'+this.bookingid+'"/></td></tr><tr><th>'
                                            +"Customer ID"+'</th><td><input type="text" disabled="true" id="customerid" value="'+this.customerid+'"/></td></tr><tr><th>'
                                            +"Booking Option"+'</th><td><input type="text" disabled="true" id="bookingoption" value="'+this.bookingoption+'"/></td></tr><tr><th>'
                                            +"Date"+'</th><td><input type="text" id="date" value="'+this.date+'"/></td></tr><tr><th>'
                                            +"Status"+'</th><td><input type="text" id="status" disabled="true" value="'+this.status+'"/></td></tr><tr><th>'
                                            +"Payment Status"+'</th><td><input type="text" id="paymentstatus" disabled="true" value="'+this.payment_status+'"/></td></tr><tr><th>'
                                            +"TypeMeal"+'</th><td><input type="text" id="typemeal" value="'+this.typemeal+'"/></td></tr><tr><th>'
                                            +"Charges"+'</th><td><input type="text" id="charges" disabled="true" value="'+this.charges+'"/></td></tr>');
                        }
                        
                        
                     });

                       $( "#date" ).datepicker({dateFormat:'yy-mm-dd'});
                    }
                    });

                    }));
                    

              $(document).ready(function(){
                      $('form#myform').submit(function(event){
                        event.preventDefault();
                        var tiffinid=$('#tiffinid').val();
                        var L=$('#L').val();
                        var D=$('#D').val();
                        var bookingid=$('#bookingid').val();
                        var customerid=$('#customerid').val();
                        var bookingoption=$('#bookingoption').val();
                        var date=$('#date').val();
                        var status=$('#status').val();
                        var paymentstatus=$('#paymentstatus').val();
                        var typemeal=$('#typemeal').val();
                        var charges=$('#charges').val();
                        var token=localStorage.getItem('token');
                    var order={
                        tiffinid: tiffinid,
                        L: L,
                        D: D,
                        bookingid: bookingid,
                        customerid: customerid,
                        bookingoption: bookingoption,
                        date: date,
                        status: status,
                        typemeal: typemeal,
                        charges: charges,
                        token:token,
                    };

            $.ajax({
                 type: 'POST',
                url: '../../api/adminupdatetiffin',
                data: order,
                success: function(elm){
                  // console.log(elm);
                if(elm.results.success==1){
                 alert('Successfully Updated!')
                    window.location='/admin/tiffins/'+bookingid;
            }
                else{
                   $a=elm.results.message;
                    $('#show').html('<p id="para" style="color:red;margin-left:10px;">'+$a+'</p>')
                   //alert('error');
                    
                }
            }
        });
                        
        }); 

        $('#logout').on('click', function(){

              var token=localStorage.getItem('token');
              var order={
                  token:token,
              };
              $.ajax({
                  type: 'POST',
                  url: '../../api/adminlogout',
                  data: order,
                  success: function(elm){
                      if(elm.results.success){
                        localStorage.removeItem('token');
                        window.location="{{url('/admin')}}";
                      }
                      else{
                        alert('Internal server Error');
                      }
                  }
          });
              
      }); 
    });
</script>
</html>                