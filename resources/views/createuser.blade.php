<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
  if(localStorage.getItem('token')==null)
      window.location="{{url('/admin')}}";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dashboard - Admin Template</title>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/theme.css')}}" />
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/style.css')}}" />

</head>

<body>  
<div id="container">
      <div id="header">
          <h2>Makemymeals admin panel</h2>
           <div id="topmenu">
              <ul>
                  <li ><a href="{{url('/admin/home')}}" >Dashboard</a></li>
                    <li><a href="{{url('/admin/orders')}}" id="orders">Orders</a></li>
                  <li class="current"><a href="{{url('/admin/users')}}">Users</a></li>
                    <li><a href="{{url('/admin/foods')}}">Foods</a></li>
                    
              </ul>
          </div>
          <button id="logout" style="padding:3px 8px;background-color:#1ab188;color:white;float:right" >LOGOUT</button>

      </div>
        <div id="error">
            
         </div
  </div>
  <div id="sidebar">
    <ul>
      
      <li><h3><a href="{{url('/admin/home')}}" class="house">Dashboard</a></h3>
          <ul>
              <li><a href="{{url('/admin/home')}}" class="report">Show Tiffins on DATE</a></li>
              <!-- <li><a href="#" class="report_seo">A</a></li> -->
              
          </ul>
      </li>
      <li><h3><a href="{{url('/admin/orders')}}" class="folder_table">Orders</a></h3>
            <ul>
              <li><a href="{{url('/admin/order/create')}}" class="addorder">Create Order</a></li>
            <li><a href="{{url('/admin/orders')}}" class="shipping">Show Orders</a></li>
              
          </ul>
      </li>
      
    <li><h3><a href="{{url('/admin/users')}}" class="user">Users</a></h3>
            <ul>
              <li><a href="{{url('/admin/user/create')}}" class="useradd">Create user</a></li>
              <li><a href="{{url('/admin/users')}}" class="group">Show Users</a></li>
              
          </ul>
      </li>

      <li><h3><a href="{{url('/admin/foods')}}" class="manage">Foods</a></h3>
            <ul>
              <li><a href="{{url('/admin/food/create')}}" class="manage_page">Create Food</a></li>
              <li><a href="{{url('/admin/foods')}}" class="cart">Show Foods</a></li>
              <!-- <li><a href="#" class="folder">Product categories</a></li>
              <li><a href="#" class="promotions">Promotions</a></li> -->
          </ul>
      </li>
  </ul>       
</div>
	
        

        <div id="wrapper">
            <div id="content">
                <div id="box">
                	<h3>Create User</h3>
                  <div id="show">
                  </div>

                     <form id="myform">
                	  <table width="100%">
                         
                          <tr><th>Name</th><td><input type="text" id="name" size="85" required/></td></tr>
                          <tr><th>Email</th><td><input type="email" id="email" size="85" required/></td></tr>
                          <tr><th>Mobile</th><td><input type="text" id="mobile" size="85" required/></td></tr>
                          <tr><th>Password</th><td><input type="password" id="password" size="85" required/></td></tr>
					   </table>  
                       <input type="submit" value="submit"/>
                       </form>
                </div>
            </div>

<script src="{{URL::asset('js/jquery.js')}}"></script>

<script>
 $(document).ready(function(){
    $('form#myform').submit(function(event){
      event.preventDefault();
      var name=$('#name').val();
      var email=$('#email').val();
      var mobile=$('#mobile').val();
      var password=$('#password').val();
      var token=localStorage.getItem('token');
      var order={
                        name: name,
                        email: email,
                        mobile: mobile,
                        password:password,
                        token: token,
                };
    

      $.ajax({
        type: 'POST',
        url: '../../api/createuser',
        data: order,
        success: function(elm){
          console.log(elm.results.error);
          if(elm.results.success==1){
            alert('Successfully Created!')
              window.location="{{url('admin/users')}}";
            
          }
          else{
                $('#show').html('<p id="para" style="color:red;" >'+elm.results.message+'</p>')
               //alert('error');
            }
         }
      });

    }); 
});

    $('#logout').on('click', function(){

            var token=localStorage.getItem('token');
            var order={
                token:token,
            };
            $.ajax({
                type: 'POST',
                url: '../../api/adminlogout',
                data: order,
                success: function(elm){
                    if(elm.results.success){
                      localStorage.removeItem('token');
                      window.location="{{url('/admin')}}";
                    }
                    else{
                      alert('Internal server Error');
                    }
                }
        });
            
      }); 
  


 </script>

    