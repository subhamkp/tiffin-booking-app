<!DOCTYPE html>
<html>
<head>
<script>
  if(localStorage.getItem('token')==null)
      window.location="{{url('/admin')}}";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dashboard - Admin Template</title>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/theme.css')}}" />
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/style.css')}}" />
<script src="{{URL::asset('js/jquery.js')}}"></script>
</head>

<body>  
<div id="container">
      <div id="header">
          <h2>Makemymeals admin panel</h2>
           <div id="topmenu">
              <ul>
                  <li ><a href="{{url('/admin/home')}}" >Dashboard</a></li>
                    <li class="current"><a href="{{url('/admin/orders')}}" id="orders">Orders</a></li>
                  <li><a href="{{url('/admin/users')}}">Users</a></li>
                    <li><a href="{{url('/admin/foods')}}">Foods</a></li>
                    
              </ul>
          </div>
          <button id="logout" style="padding:3px 8px;background-color:#1ab188;color:white;float:right" >LOGOUT</button>

      </div>
        <div id="error">
            
         </div
  </div>
  <div id="sidebar">
    <ul>
      
      <li><h3><a href="{{url('/admin/home')}}" class="house">Dashboard</a></h3>
          <ul>
              <li><a href="{{url('/admin/home')}}" class="report">Show Tiffins on DATE</a></li>
              <!-- <li><a href="#" class="report_seo">A</a></li> -->
              
          </ul>
      </li>
      <li><h3><a href="{{url('/admin/orders')}}" class="folder_table">Orders</a></h3>
            <ul>
              <li><a href="{{url('/admin/order/create')}}" class="addorder">Create Order</a></li>
            <li><a href="{{url('/admin/orders')}}" class="shipping">Show Orders</a></li>
              
          </ul>
      </li>
      
    <li><h3><a href="{{url('/admin/users')}}" class="user">Users</a></h3>
            <ul>
              <li><a href="{{url('/admin/user/create')}}" class="useradd">Create user</a></li>
              <li><a href="{{url('/admin/users')}}" class="group">Show Users</a></li>
              
          </ul>
      </li>

      <li><h3><a href="{{url('/admin/foods')}}" class="manage">Foods</a></h3>
            <ul>
              <li><a href="{{url('/admin/food/create')}}" class="manage_page">Create Food</a></li>
              <li><a href="{{url('/admin/foods')}}" class="cart">Show Foods</a></li>
              <!-- <li><a href="#" class="folder">Product categories</a></li>
              <li><a href="#" class="promotions">Promotions</a></li> -->
          </ul>
      </li>
  </ul>       
</div>
	

		<div id="wrapper">
            <div id="content">
                <div id="box">
                	<h3>Order details</h3>
                    <form id="myform">
                    <table id="table">
                         
                    </table>
                    <input type="submit" value="Update Order"/>
                    </form>

                    <script>	
                    $(document).ready((function(){
                    var $o = $('#table');
                    var a={{$bookingid}};
                    var token=localStorage.getItem('token');
                    var order={
                        token:token ,
                  };
    
                    
                    $.ajax({
                    type: 'POST',
                    url: '../../../api/showbookings',
                    dataType: 'json',
                    data:order,
                    success: function(elm){
                        
                    $.each(elm.results.bookings, function(){
                        
                        if(this.bookingid==a){
                        $o.append('<tr><th>'+"Booking ID"+'</th><td><input type="text" id="bookingid" disabled="true" value="'+this.bookingid+'"/></td></tr><tr><th>'
                                            +"Name"+'</th><td><input type="text" id="name" value="'+this.name+'"/></td></tr><tr><th>'
                                            +"Address"+'</th><td><input type="text" id="address" value="'+this.address+'"/></td></tr><tr><th>'
                                            +"L"+'</th><td><input type="text" id="L" disabled="true" value="'+this.L+'"/></td></tr><tr><th>'
                                            +"D"+'</th><td><input type="text" id="D" disabled="true"  value="'+this.D+'"/></td></tr><tr><th>'
                                            +"User Mobile"+'</th><td><input type="text" id="usermobile" disabled="true"  value="'+this.usermobile+'"/></td></tr><tr><th>'
                                            +"Delivery Mobile"+'</th><td><input type="text" id="deliverymobile" value="'+this.deliverymobile+'"/></td></tr><tr><th>'
                                            +"Booking Option"+'</th><td><input type="text" id="bookingoption" disabled="true" value="'+this.bookingoption+'"/></td></tr><tr><th>'
                                            +"Starting Date"+'</th><td><input type="text" id="starting_date" disabled="true"  value="'+this.starting_date+'"/></td></tr><tr><th>'
                                            +"Quantity"+'</th><td><input type="text" id="no_of_subscriptions" disabled="true"  value="'+this.no_of_subscriptions+'"/></td></tr><tr><th>'
                                            +"Created At"+'</th><td><input type="text" id="created_at" disabled="true"  value="'+this.created_at+'"/></td></tr><tr><th>'
                                            +"Updated At"+'</th><td><input type="text" id="updated_at" disabled="true"  value="'+this.updated_at+'"/></td></tr>');
                        }
                        
                        
                     });
                    }
                    });
                    }));

                    $(document).ready(function(){
                      $('form#myform').submit(function(event){
                        event.preventDefault();
                        var bookingid=$('#bookingid').val();
                        var name=$('#name').val();
                        var address=$('#address').val();
                        var L=$('#L').val();
                        var D=$('#D').val();
                        var usermobile=$('#usermobile').val();
                        var deliverymobile=$('#deliverymobile').val();
                        var bookingoption=$('#bookingoption').val();
                        var starting_date=$('#starting_date').val();
                        var no_of_subscriptions=$('#no_of_subscriptions').val();
                        var token=localStorage.getItem('token');
              
                    var order={
                        bookingid: bookingid,
                        name: name,
                        address: address,
                        L: L,
                        D: D,
                        usermobile: usermobile,
                        deliverymobile: deliverymobile,
                        bookingoption: bookingoption,
                        starting_date: starting_date,
                        no_of_subscriptions: no_of_subscriptions,
                        token:token,
                    };
    

            $.ajax({
                 type: 'POST',
                url: '../../../api/updatebooking',
                data: order,
                success: function(elm){

                if(elm.success==0){
                    $('#show').append('<p id="para" style="color:red;" >Unable to update,please try again!</p>')
            //alert('error');
            }
                else{
                    alert('Successfully Updated!')
                    window.location="{{url('/admin/orders')}}";
                }
            }
        });
                        
        }); 
                    });

        $('#logout').on('click', function(){

              var token=localStorage.getItem('token');
              var order={
                  token:token,
              };
              $.ajax({
                  type: 'POST',
                  url: '../../../api/adminlogout',
                  data: order,
                  success: function(elm){
                      if(elm.results.success){
                        localStorage.removeItem('token');
                        window.location="{{url('/admin')}}";
                      }
                      else{
                        alert('Internal server Error');
                      }
                  }
          });
              
      }); 
    
</script> 
                    
                
                