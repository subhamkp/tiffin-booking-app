<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
  if(localStorage.getItem('token')==null)
      window.location="{{url('/admin')}}";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dashboard - Admin Template</title>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/theme.css')}}" />
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/style.css')}}" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
<script>
    $(function() {
        $( "#datepicker" ).datepicker({dateFormat:'yy-mm-dd'});
    });
</script> 

<script>
  $(document).ready(function(){
      $(document).on('click',".w3-bar-item",function(){
          $(this).parent().next("div").slideToggle("slow");
          $(this).parent().next("div").removeClass("s");
          $('.s').hide();
          $(this).parent().next("div").addClass("s");
      });
  });
</script>

<style>
.w3-bar{width:700px;overflow:hidden;border:1px solid #ccc;color:#000!important;background-color:#f3f3f3!important;}
.w3-bar-item{padding:8px 16px;float:left;border:none;outline:none;display:block}
.w3-bar-item-right{padding:8px 16px;float:right;border:none;outline:none;display:block}
td{padding:10px;} tr:hover{background-color:#f1f1f1;}
.update{color:#1ab188;}
</style>

<style>
  .btn {
  font-size: 3vmin;
  padding: 0.75em 1.5em;
  background-color: #fff;
  border: 1px solid #bbb;
  color: #333;
  text-decoration: none;
  display: inline;
  border-radius: 4px;
  -webkit-transition: background-color 1s ease;
  -moz-transition: background-color 1s ease;
  transition: background-color 1s ease;
}

.btn:hover {
  background-color: #ddd;
  -webkit-transition: background-color 1s ease;
  -moz-transition: background-color 1s ease;
  transition: background-color 1s ease;
}

.btn-small {
  padding: .75em 1em;
  font-size: 0.8em;
}

.modal-box {
  display: none;
  position: absolute;
  z-index: 1000;
  width: 99%;
  background: white;
  border-bottom: 1px solid #aaa;
  border-radius: 4px;
  box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
  border: 1px solid rgba(0, 0, 0, 0.1);
  background-clip: padding-box;
}
@media (min-width: 32em) {

.modal-box { width: 70%; }
}

.modal-box header,
.modal-box .modal-header {
  padding: 1.25em 1.5em;
  border-bottom: 1px solid #ddd;
}

.modal-box header h3,
.modal-box header h4,
.modal-box .modal-header h3,
.modal-box .modal-header h4 { margin:1; }

.modal-box .modal-body { padding: 2em 1.5em; }

.modal-box footer,
.modal-box .modal-footer {
  padding: 1.25em 1.5em;
  border-top: 1px solid #ddd;
  background: rgba(0, 0, 0, 0.02);
  text-align: right;
}

.modal-overlay {
  opacity: 0;
  filter: alpha(opacity=0);
  position: absolute;
  top: 0;
  left: 0;
  z-index: 900;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.3) !important;
}

a.close {
  line-height: 1;
  font-size: 1.5em;
  position: absolute;
  top: 5%;
  right: 2%;
  text-decoration: none;
  color: #bbb;
}

a.close:hover {
  color: #222;
  -webkit-transition: color 1s ease;
  -moz-transition: color 1s ease;
  transition: color 1s ease;
}

</style>

</head>
<body>


<div id="popup" class="modal-box">
  <header> <a href="#" class="btn btn-small js-modal-close  close cancel" style="top:1.5px;padding:10px 19px;">x</a>
    <h3 id="phead" style="text-transform: uppercase;font-size: 1.2em;color:#1ab188"></h3>
  </header>
  <div class="modal-body">  
    <table style="text-align:center;" id="ptable"></h3>
            <thead>
              <tr>
                <th width="40px"><a href="#">Order ID</a></th>
                <th width="30px"><a href="#" >Name</a></th>
                <th width="30px"><a href="#" >Address</a></th>
                <th width="10px"><a href="#">User Mobile</a></th>
                <th width="10px"><a href="#">Delivery Mobile</a></th>
                <th width="100px"><a href="#">Type of Meal</a></th>
                <th width="60px"><a href="#">Status</a></th>
                <th width="25px"><a href="#">Payment Status</a></th>
                <th width="25px"><a href="#">Change Status</a></th>
              </tr>
            </thead>
            <tbody id="ptiffins">
              
            </tbody>
          </table>
  </div>
  <footer> <a href="#" class="btn btn-small js-modal-close cancel" style="top:0px;padding:13px;">Close</a> </footer>
</div>


<div id="nomodal">
<div id="container">
    	<div id="header">
        	<h2>Makemymeals admin panel</h2>
           <div id="topmenu">
            	<ul>
                	<li class="current"><a href="{{url('/admin/home')}}" >Dashboard</a></li>
                    <li><a href="{{url('/admin/orders')}}" id="orders">Orders</a></li>
                	<li><a href="{{url('/admin/users')}}">Users</a></li>
                    <li><a href="{{url('/admin/foods')}}">Foods</a></li>
                    
              </ul>
          </div>
          <button id="logout" style="padding:3px 8px;background-color:#1ab188;color:white;float:right" >LOGOUT</button>

      </div>
        <div id="error">
            
         </div
	</div>

	<div id="sidebar">
    <ul>
      
      <li><h3><a href="{{url('/admin/home')}}" class="house">Dashboard</a></h3>
          <ul>
              <li><a href="{{url('/admin/home')}}" class="report">Show Tiffins on DATE</a></li>
              <!-- <li><a href="#" class="report_seo">A</a></li> -->
              
          </ul>
      </li>
      <li><h3><a href="{{url('/admin/orders')}}" class="folder_table">Orders</a></h3>
            <ul>
              <li><a href="{{url('/admin/order/create')}}" class="addorder">Create Order</a></li>
            <li><a href="{{url('/admin/orders')}}" class="shipping">Show Orders</a></li>
              
          </ul>
      </li>
      
    <li><h3><a href="{{url('/admin/users')}}" class="user">Users</a></h3>
            <ul>
              <li><a href="{{url('/admin/user/create')}}" class="useradd">Create user</a></li>
              <li><a href="{{url('/admin/users')}}" class="group">Show Users</a></li>
              
          </ul>
      </li>

      <li><h3><a href="{{url('/admin/foods')}}" class="manage">Foods</a></h3>
            <ul>
              <li><a href="{{url('/admin/food/create')}}" class="manage_page">Create Food</a></li>
              <li><a href="{{url('/admin/foods')}}" class="cart">Show Foods</a></li>
              <!-- <li><a href="#" class="folder">Product categories</a></li>
              <li><a href="#" class="promotions">Promotions</a></li> -->
          </ul>
      </li>
  </ul>       
</div>
          
          
     

    
		 
<div id="wrapper">
  <div id="content">
    <div id="box">
      <h3 style="padding:10px;margin:5px;">Tiffins on date</h3>
      <h3 style="padding:10px;margin:5px;">Date: <input type="text" id="datepicker" ></h3><h3 style="padding:10px;margin:5px;"><input type="checkbox" value="1" name="meal" id="lunch"><label for="lunch"> Lunch</label></h3><h3 style="padding:10px;margin:5px;"><input type="checkbox" value="1" name="meal" id="dinner"><label for="dinner"> Dinner</label></h3>
      <button id="go" style="padding:3px 8px;margin:6px;color:#1ab188;" >GO!</button>
        
      <h3 style="margin-bottom:20px;">Tiffins</h3>

      <!-- start Don't touch-->
      <div id="display1"></div>
      <div id="display2"></div>
      <!-- end -->
    </div>
  </div>
</div>

</div> 
</div>              
<script>
var lunch;
var dinner;
var date;
$(function(){
  
      var $tiffins= $('#table');
      $('#go').on('click', function(e){
        e.preventDefault();
            date=$('#datepicker').val();

            if(date=="")
                {
                  alert('Enter a Valid date');
                  return;
                }
            if($('#lunch').is(":checked"))
              {
                 lunch=1;
              }
              else{

                  lunch=0;
              }
            if ($('#dinner').is(":checked"))
              {
                 dinner=1
              }
              else{
                  dinner=0;
              }

        $('#display1').html("");
        $('#display2').html("");

        if(!(lunch||dinner)){alert('Please check either lunch or dinner');return;}
        var token=localStorage.getItem('token');

        $t="";
        if(lunch==1){
            var order={
                date: date,
                token:token,
                L:lunch,
            };

            $t+='<h3 style="margin-bottom:20px">LUNCH</h3>';
            $.ajax({
                type: 'POST',
                url: '../api/mealsperday',
                data: order,
                success: function(results){
                    console.log(results);
                    
                    $.each(results.results, function(){
                        $dis="";$dis1="";$dis2="";$dis3="";
                        if(this.status=="0"){$dis="disabled";}
                        if(this.status=="pending"){$dis1="disabled";$dis3="disabled";}
                        if(this.status=="packed"){$dis1="disabled";$dis2="disabled";}
                        if(this.status=="delivered"){$dis1="disabled";$dis2="disabled";$dis3="disabled";}
                        $t+='<div style="margin-bottom:20px" id="l"><div class="w3-bar" style="margin:auto;">\
                        <div class="w3-bar-item">'+this.name+' </div>\
                        <div class="w3-bar-item-right"><button class="update" value="L'+this.foodid+'" type="button">'
                        +"Update"+'</button></div>\
                        <select '+$dis+' class="w3-bar-item-right"  id="selectL'+this.foodid+'"><option '+$dis1+' value="pending">'
                        +"Pending"+'</option><option '+$dis2+' value="packed">'
                        +"Packed"+'</option><option '+$dis3+' value="delivered">'
                        +"Delivered"+'</option></select>\
                        <div class="w3-bar-item" style="float:right;"><span id='+this.foodid+'>'
                        +this.status+'</span></div></div><div style="display:none;margin-bottom:20px;" class="s">\
                        <table id="table" style="text-align:center;"><tr></tr>\
                        <tr><td><a class="js-open-modal" href="#" data-modal-id="L1" mealtype="'+this.foodid+'">'+"Sufficiant Balance"+'</a></td><td>'+this.sufficientcount+'</td></tr>\
                        <tr><td><a class="js-open-modal" href="#" data-modal-id="L2" mealtype="'+this.foodid+'">'+"Insufficiant Balance"+'</a></td><td>'+this.insufficientcount+'</td>\
                        <tr><td><a class="js-open-modal" href="#" data-modal-id="L3" mealtype="'+this.foodid+'">'+"Credit"+'</a></td><td>'+this.creditcount+'</td>\
                        <tr><td><a class="js-open-modal" href="#" data-modal-id="L4" mealtype="'+this.foodid+'">'+"Cancelled"+'</a></td><td>'+this.cancelledcount+'</td></tr> <tr><td><a class="js-open-modal" href="#" data-modal-id="L5" mealtype="'+this.foodid+'">'+"Paid"+'</a></td><td>'+this.paidcount+'</td></tr></table></div></div>';

                });
                $('#display1').html($t);
                $t="";
                $t+='<h3 style="margin-bottom:20px">LUNCH</h3>';
                
            }
            });
        }
        $m="";
        if(dinner==1){

            var order={
                date:date,
                token:token,
                D:dinner,
            };
            $m+='<h3 style="margin-bottom:20px">DINNER</h3>';
            $.ajax({
                type: 'POST',
                url: '../api/mealsperday',
                data: order,
                success: function(results){
                    $.each(results.results, function(){
                        $dis="";$dis1="";$dis2="";$dis3="";
                        if(this.status=="0"){$dis="disabled";}
                        if(this.status=="pending"){$dis1="disabled";$dis3="disabled";}
                        if(this.status=="packed"){$dis1="disabled";$dis2="disabled";}
                        if(this.status=="delivered"){$dis1="disabled";$dis2="disabled";$dis3="disabled";}
                        $m+='<div style="margin-bottom:20px" id="d"><div class="w3-bar" style="margin:auto;">\
                        <div class="w3-bar-item">'+this.name+'</div>\
                        <div class="w3-bar-item-right"><button class="update" value="D'+this.foodid+'" type="button">'
                        +"Update"+'</button></div>\
                        <select '+$dis+' class="w3-bar-item-right"  id="selectD'+this.foodid+'"><option '+$dis1+' value="pending">'
                        +"Pending"+'</option><option '+$dis2+' value="packed">'
                        +"Packed"+'</option><option '+$dis3+ ' value="delivered">'
                        +"Delivered"+'</option></select>\
                        <div class="w3-bar-item" style="float:right;"><span id='+this.foodid+'>'
                        +this.status+'</span></div></div><div style="display:none;margin-bottom:20px;" class="s">\
                        <table id="table" style="text-align:center;"><tr></tr>\
                        <tr><td><a class="js-open-modal" href="#" data-modal-id="D1" mealtype="'+this.foodid+'">'+"Sufficiant Balance"+'</a></td><td>'+this.sufficientcount+'</td></tr>\
                        <tr><td><a class="js-open-modal" href="#" data-modal-id="D2" mealtype="'+this.foodid+'">'+"Insufficiant Balance"+'</a></td><td>'+this.insufficientcount+'</td>\
                        <tr><td><a class="js-open-modal" href="#" data-modal-id="D3" mealtype="'+this.foodid+'">'+"Credit"+'</a></td><td>'+this.creditcount+'</td>\
                        <tr><td><a class="js-open-modal" href="#" data-modal-id="D4" mealtype="'+this.foodid+'">'+"Cancelled"+'</a></td><td>'+this.cancelledcount+'</td></tr> <tr><td><a class="js-open-modal" href="#" data-modal-id="D5" mealtype="'+this.foodid+'">'+"Paid"+'</a></td><td>'+this.paidcount+'</td></tr></table></div></div>';

                });
                $('#display2').html($m); 
                $m="";
                $m+='<h3 style="margin-bottom:20px">DINNER</h3>'; 
                
            }
            });     
        }
  });
        
        

      $('#logout').on('click', function(){

                var token=localStorage.getItem('token');
                var order={
                    token:token,
                };
                $.ajax({
                    type: 'POST',
                    url: '../api/adminlogout',
                    data: order,
                    success: function(elm){
                        if(elm.results.success){
                          localStorage.removeItem('token');
                          window.location="{{url('/admin/home')}}";
                        }
                        else{
                          alert('Internal server Error');
                        }
                    }
            });            
      });


   });

  $(document).on('click','.update',function(e){
      // e.preventDefault();
        
        var el=$(this).val();
        var foodid=el.slice(1);
        var mealtime=el[0];

        var select="#select"+mealtime+foodid+" option:selected";

        // alert(mealtime);
        
        var selected=$(select).val();

        // alert(selected);
        
        $('#'+foodid).html(selected);
        var token=localStorage.getItem('token');
        var order={
                  token:token,
                  selected:selected,
                  foodid:foodid,
                  mealtime:mealtime,
                  date: date,
              };
              $.ajax({
                  type: 'POST',
                  url: '../api/changestatus',
                  data: order,
                  success: function(elm){
                      // console.log(elm);

                      if(elm.results.success==1){
                        alert(elm.results.message);
                      }
                      else{
                        alert(elm.results.message);
                      }
                  }
          });

         $('#go').click();
    });

</script>  
<script>

$(function(){

  var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

  $(document).on('click','a[data-modal-id]',function(e) {
    e.preventDefault();
    var modal = $(this).attr('data-modal-id');

    checkstatus(this); // see function below

    $("body").append(appendthis);
    $(window).resize();
    $(".modal-overlay").fadeTo(500, 0.7);
    $('#popup').fadeIn($(this).data());
  });  
  
  
  $(".js-modal-close, .modal-overlay").click(function(e) {
      e.preventDefault();
      $(".modal-box, .modal-overlay").fadeOut(500, function() {
          $(".modal-overlay").remove();
      });
   });
 
  $(window).resize(function() {
      var scrollTop = $(window).scrollTop();
      $(".modal-overlay").height($(document).height());
      $(".modal-box").css({
          top:scrollTop+10,
          left: ($(window).width() - $(".modal-box").outerWidth()) / 2
        });
      }); 


    $(document).on('click','.cancel',function(){
      $('#go').click();
    });
});
  
  function checkstatus(elm) //461
   {
      var typemeal=$(elm).attr('mealtype');
      console.log(typemeal);
      var e=$(elm).attr('data-modal-id');
      var mealtime=e[0];
      var paymentstatus=e[1];
      var token=localStorage.getItem('token');
      var order={
        typemeal:typemeal,
        mealtime:mealtime,
        paymentstatus:paymentstatus,
        token:token,
        date:date
      }
      if(paymentstatus==1)$p="sufficient";
      if(paymentstatus==2)$p="insufficient";
      if(paymentstatus==3)$p="credit";
      if(paymentstatus==4)$p="cancelled";
      if(paymentstatus==5)$p="paid";
      $('#phead').html($p);

      $t="";
      $.ajax({
                  type: 'POST',
                  url: '../api/checkstatus',
                  data: order,
                  success: function(elm){
                  if(elm.results.success==1){
                    $('#phead').append(' ( '+elm.results.tiffincount+' ) ');
                    
                    $.each(elm.results.tiffins, function(){
                        console.log(this);
                        $t+='<tr id="pop'+this.tiffinid+'"><td>'+this.bookingid+'</td><td>'+this.name+'</td><td>'+this.address+'</td><td>'+this.usermobile+'</td><td>'+this.deliverymobile+'</td><td>'+this.typemeal+'</td><td>'+this.status+'</td><td>'+elm.results.paymentstatus+'</td><td style="width:10px;"><select style="padding:-10px;" id="indselect'+this.tiffinid+'"><option value="credit">'
                        +"Credit"+'</option><option value="cancelled">'
                        +"Cancel"+'</option></select></td><td style="width:10px;"><button paymentstatus="'+elm.results.paymentstatus+'" class="indupdate" id="'+this.tiffinid+'" value="'+this.tiffinid+'">Update</button></td></tr>';                        
                     });

                    $('#ptiffins').html($t);
                    }

                    else{
                      alert(elm.results.message);
                    }
                  }
        });
            
      } 

      $(document).on('click','.indupdate',function(e) {
        var tiffinid=$(this).val();
        var paymentstatus=$(this).attr('paymentstatus');
        var select="#indselect"+tiffinid+" option:selected";
        var selected=$(select).val();
        var token=localStorage.getItem('token');
        var order={
                  token:token,
                  selected:selected,
                  tiffinid:tiffinid,
              };
              $.ajax({
                  type: 'POST',
                  url: '../api/updatepaymentstatussingle',
                  data: order,
                  success: function(elm){
                      

                      if(elm.results.success==1){
                        // alert(selected);

                        if(elm.results.changepaymentstatus!=paymentstatus)
                        {
                          $('#pop'+tiffinid).fadeOut('slow');
                        }
                        // alert(elm.results.message);
                      }
                      else{
                        alert(elm.results.message);
                      }
                  }
          });

        
  });

   
</script>
</div>
				  
</body>
			