<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
  if(localStorage.getItem('token')==null)
      window.location="{{url('')}}";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dashboard - Admin Template</title>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/theme.css')}}" />
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/style.css')}}" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>  
<div id="container">
      <div id="header">
          <h2>Makemymeals admin panel</h2>
           <div id="topmenu">
              <ul>
                  <li ><a href="{{url('/admin/home')}}" >Dashboard</a></li>
                    <li><a href="{{url('/admin/orders')}}" id="orders">Orders</a></li>
                  <li class="current"><a href="{{url('/admin/users')}}">Users</a></li>
                    <li><a href="{{url('/admin/foods')}}">Foods</a></li>
                    
              </ul>
          </div>
          <button id="logout" style="padding:3px 8px;background-color:#1ab188;color:white;float:right" >LOGOUT</button>

      </div>
        <div id="error">
            
         </div
  </div>
  <div id="sidebar">
    <ul>
      
      <li><h3><a href="{{url('/admin/home')}}" class="house">Dashboard</a></h3>
          <ul>
              <li><a href="{{url('/admin/home')}}" class="report">Show Tiffins on DATE</a></li>
              <!-- <li><a href="#" class="report_seo">A</a></li> -->
              
          </ul>
      </li>
      <li><h3><a href="{{url('/admin/orders')}}" class="folder_table">Orders</a></h3>
            <ul>
              <li><a href="{{url('/admin/order/create')}}" class="addorder">Create Order</a></li>
            <li><a href="{{url('/admin/orders')}}" class="shipping">Show Orders</a></li>
              
          </ul>
      </li>
      
    <li><h3><a href="{{url('/admin/users')}}" class="user">Users</a></h3>
            <ul>
              <li><a href="{{url('/admin/user/create')}}" class="useradd">Create user</a></li>
              <li><a href="{{url('/admin/users')}}" class="group">Show Users</a></li>
              
          </ul>
      </li>

      <li><h3><a href="{{url('/admin/foods')}}" class="manage">Foods</a></h3>
            <ul>
              <li><a href="{{url('/admin/food/create')}}" class="manage_page">Create Food</a></li>
              <li><a href="{{url('/admin/foods')}}" class="cart">Show Foods</a></li>
              <!-- <li><a href="#" class="folder">Product categories</a></li>
              <li><a href="#" class="promotions">Promotions</a></li> -->
          </ul>
      </li>
  </ul>       
</div>
	

		<div id="wrapper">
            <div id="content">
                <div id="box">
                	<h3>User details</h3>
                  <div id="show">
                  </div>
                    <form id="myform" >
                    <table id="table" width=100% >
                         
                    </table>
                    <input type="submit" value="Update User"/>
                    </form>

                    
                
                <form id="myform1">
                     <input type="hidden" id="id" value={{$id}} />
                     <input type="submit" value="Delete user"/>

                 </form>

                
                  
                    
                    

<script>  
        $(document).ready((function(){
                    var $o = $('#table');
                    var a={{$id}};
                    
                    var token=localStorage.getItem('token');
                    var order={
                        token:token ,
                      }
                    $.ajax({
                    type: 'POST',
                    url: '../../../api/showusers',
                    dataType: 'json',
                    data: order,
                    success: function(elm){
           
                    $.each(elm.results.users, function(){
                        
                        if(this.id==a){
                        $o.append('<tr><th>'+"ID"+'</th><td><input type="text" disabled="true"  id="id" value="'+this.id+'"/></td></tr><tr><th>'
                                            +"Name"+'</th><td><input type="text" id="name" value="'+this.name+'"/></td></tr><tr><th>'
                                            +"Email"+'</th><td><input type="text" id="email" value="'+this.email+'"/></td></tr><tr><th>'
                                            +"Mobile"+'</th><td><input type="text" disabled="true" id="mobile" value="'+this.mobile+'"/></td></tr><tr><th>'
                                            +"Verification status"+'</th><td><input type="text" disabled="true" id="isVerified" value="'+this.isVerified+'"/></td></tr><tr><th>'
                                            +"Created At"+'</th><td>'+this.created_at+'</td></tr><tr><th>'
                                            +"Updated At"+'</th><td>'+this.updated_at+'</td></tr>');
                        }
                        
                        
                     });
                    }
                    });
                    }));

          $(document).ready(function(){
                $('form#myform').submit(function(event){
                        event.preventDefault();
                        var id=$('#id').val();
                        var name=$('#name').val();
                        var email=$('#email').val();
                        var mobile=$('#mobile').val();
                        var isVerified=$('#isVerified').val();
                        var token=localStorage.getItem('token');
                    
                    var order={
                        id: id,
                        name: name,
                        email: email,
                        mobile: mobile,
                        isVerified: isVerified,
                        token:token,
                    };
                // console.log(order);

            $.ajax({
                 type: 'POST',
                url: '../../../api/updateuser',
                data: order,
                success: function(elm){
                  // console.log(elm);
                if(elm.results.success==1){
                    alert('Successfully Updated!')
                    window.location="{{url('admin/users')}}";
            }
                else{
                  $('#show').html('<p id="para" style="color:red;" >'+elm.results.message+'</p>')
                    
                }
            }
        });

            });  
        });







    $(document).ready(function(){
                     
            $('form#myform1').submit(function(event){
                  event.preventDefault();
                  
                  var id=$('#id').val();
                  var token=localStorage.getItem('token');
                    

                    var order={
                        id: id,
                        token:token,

                    };
    

            $.ajax({
                 type: 'POST',
                url: '../../../api/deleteuser',
                data: order,
                success: function(elm){
                  // console.log(elm);
                if(elm.results.success==1){
                  alert('Successfully Deleted!')
                    window.location="{{url('admin/users')}}";
            }
                else{
                    $('#show').html('<p id="para" style="color:red;" >'+elm.elements.message+'</p>')
                }
            }
        });


      });

      $('#logout').on('click', function(){

              var token=localStorage.getItem('token');
              var order={
                  token:token,
              };
              $.ajax({
                  type: 'POST',
                  url: '../../../api/adminlogout',
                  data: order,
                  success: function(elm){
                      if(elm.results.success){
                        localStorage.removeItem('token');
                        window.location="{{url('/admin')}}";
                      }
                      else{
                        alert('Internal server Error');
                      }
                  }
          });
              
      });  
  });
</script> 