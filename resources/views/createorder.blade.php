<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
  if(localStorage.getItem('token')==null)
      window.location="{{url('')}}";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dashboard - Admin Template</title>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/theme.css')}}" />
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/style.css')}}" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
<script>
    $( function() {
        $( "#starting_date" ).datepicker({dateFormat:'yy-mm-dd'});
    } );
</script>

</head>

<body>  
<div id="container">
      <div id="header">
          <h2>Makemymeals admin panel</h2>
           <div id="topmenu">
              <ul>
                  <li ><a href="{{url('/admin/home')}}" >Dashboard</a></li>
                    <li class="current"><a href="{{url('/admin/orders')}}" id="orders">Orders</a></li>
                  <li><a href="{{url('/admin/users')}}">Users</a></li>
                    <li><a href="{{url('/admin/foods')}}">Foods</a></li>
                    
              </ul>
          </div>
          <button id="logout" style="padding:3px 8px;background-color:#1ab188;color:white;float:right" >LOGOUT</button>

      </div>
        <div id="error">
            
         </div
  </div>
  <div id="sidebar">
    <ul>
      
      <li><h3><a href="{{url('/admin/home')}}" class="house">Dashboard</a></h3>
          <ul>
              <li><a href="{{url('/admin/home')}}" class="report">Show Tiffins on DATE</a></li>
              <!-- <li><a href="#" class="report_seo">A</a></li> -->
              
          </ul>
      </li>
      <li><h3><a href="{{url('/admin/orders')}}" class="folder_table">Orders</a></h3>
            <ul>
              <li><a href="{{url('/admin/order/create')}}" class="addorder">Create Order</a></li>
            <li><a href="{{url('/admin/orders')}}" class="shipping">Show Orders</a></li>
              
          </ul>
      </li>
      
    <li><h3><a href="{{url('/admin/users')}}" class="user">Users</a></h3>
            <ul>
              <li><a href="{{url('/admin/user/create')}}" class="useradd">Create user</a></li>
              <li><a href="{{url('/admin/users')}}" class="group">Show Users</a></li>
              
          </ul>
      </li>

      <li><h3><a href="{{url('/admin/foods')}}" class="manage">Foods</a></h3>
            <ul>
              <li><a href="{{url('/admin/food/create')}}" class="manage_page">Create Food</a></li>
              <li><a href="{{url('/admin/foods')}}" class="cart">Show Foods</a></li>
              <!-- <li><a href="#" class="folder">Product categories</a></li>
              <li><a href="#" class="promotions">Promotions</a></li> -->
          </ul>
      </li>
  </ul>       
</div>

        <div id="wrapper">
            <div id="content">
                <div id="box">
                	<h3>Create order</h3>
                  <div id="show">
                  </div>
                	  <form id="myform">
                        <table width="100%" >
                          <tr><th>Name</th><td><input type="text" id="name" size="85" required/></td></tr>
                          <tr><th>Address</th><td><input type="text" id="address" size="85" required/></td></tr>
                          <tr><th><label for="L">L</label></th><td><input type="checkbox" name="L" id="L" size="85"></td></tr>
                          <tr><th><label for="D">D</label></th><td><input type="checkbox" name="D" id="D" size="85"/></td></tr>
                          <tr><th>User Mobile</th><td><input id="usermobile" type="text" size="85" required/></td></tr>
                          <tr><th>Delivery Mobile</th><td><input type="text" id="deliverymobile" size="85" required/></td></tr>
                          <tr><th>Booking Option</th><td><select id = "bookingoption"><option value = "weekly">Weekly</option><option value = "monthly">Monthly</option></select></td></tr>
                          <tr><th>Type of Meal</th><td><select id="typemeal"></select></td></tr>
            <script>	
                    $(document).ready((function(){
                    var $o = $('#typemeal');
                    var token=localStorage.getItem('token');
                    var order={
                        token:token ,
                      }

                    $.ajax({
                    type: 'POST',
                    url: '../../api/food',
                    dataType: 'json',
                    data: order,
                    success: function(elm){
           
                        $.each(elm.results.food, function(){
                        $a=this.name;
                        $d=this.foodid;
                        $o.append('<option value='+$d+'>'+$a+'</option>');
               
                         });
        
                      }
                    });
                }));
          </script>

                          <tr><th>Starting Date</th><td><input type="text" id="starting_date" size="85" required/></td></tr>
                          <tr><th>Quantity</th><td><input type="text" id="no_of_subscriptions" size="85" required/></td></tr>
					   </table> 
                       <input type="submit" value="Create Order"/>
                    </form> 
                </div>
            </div>
        </div>

<script>
 $(document).ready(function(){
    $('form#myform').submit(function(event){
      event.preventDefault();

      if($("#L").is(':checked')){
          $('#L').val('1');
      }
      else{
        $('#L').val('0');
      }

      if($("#D").is(':checked')){
          $('#D').val('1');
      }
      else{
        $('#D').val('0');
      }

      var name=$('#name').val();
      var address=$('#address').val();
      var L=$('#L').val();
      var D=$('#D').val();
      var usermobile=$('#usermobile').val();
      var deliverymobile=$('#deliverymobile').val();
      var bookingoption=$('#bookingoption').val();
      var typemeal=$('#typemeal').val();
      var starting_date=$('#starting_date').val();
      var items=$('#no_of_subscriptions').val();
       var token=localStorage.getItem('token');
      var order={
                        name: name,
                        address: address,
                        L: L,
                        D:D,
                        usermobile: usermobile,
                        deliverymobile: deliverymobile,
                        bookingoption: bookingoption,
                        typemeal: typemeal,
                        starting_date: starting_date,
                        no_of_subscriptions: items,
                        token:token ,
                };
    

      $.ajax({
        type: 'POST',
        url: '../../api/admincreatebooking',
        data: order,
        success: function(elm){
            // console.log(elm);
          if(elm.results.success==1){
            alert('Successfully Created!')
            window.location="{{url('/admin/orders')}}"; 
          }
          else{
              $('#show').html('<p id="para" style="color:red;" >'+elm.results.message+'</p>')
            //alert('error');
            }
         }
      });

    });  

        $('#logout').on('click', function(){

              var token=localStorage.getItem('token');
              var order={
                  token:token,
              };
              $.ajax({
                  type: 'POST',
                  url: '../api/adminlogout',
                  data: order,
                  success: function(elm){
                      if(elm.results.success){
                        localStorage.removeItem('token');
                        window.location="{{url('')}}";
                      }
                      else{
                        alert('Internal server Error');
                      }
                  }
          });
              
      });
  });


 
    </script>

    