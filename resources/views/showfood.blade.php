<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
  if(localStorage.getItem('token')==null)
      window.location="{{url('/admin')}}";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dashboard - Admin Template</title>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/theme.css')}}" />
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/style.css')}}" />

</head>

<body>  
<div id="container">
      <div id="header">
          <h2>Makemymeals admin panel</h2>
           <div id="topmenu">
              <ul>
                  <li ><a href="{{url('/admin/home')}}" >Dashboard</a></li>
                    <li><a href="{{url('/admin/orders')}}" id="orders">Orders</a></li>
                  <li><a href="{{url('/admin/users')}}">Users</a></li>
                    <li class="current"><a href="{{url('/admin/foods')}}">Foods</a></li>
                    
              </ul>
          </div>
          <button id="logout" style="padding:3px 8px;background-color:#1ab188;color:white;float:right" >LOGOUT</button>

      </div>
        <div id="error">
            
         </div
  </div>
  <div id="sidebar">
    <ul>
      
      <li><h3><a href="{{url('/admin/home')}}" class="house">Dashboard</a></h3>
          <ul>
              <li><a href="{{url('/admin/home')}}" class="report">Show Tiffins on DATE</a></li>
              <!-- <li><a href="#" class="report_seo">A</a></li> -->
              
          </ul>
      </li>
      <li><h3><a href="{{url('/admin/orders')}}" class="folder_table">Orders</a></h3>
            <ul>
              <li><a href="{{url('/admin/order/create')}}" class="addorder">Create Order</a></li>
            <li><a href="{{url('/admin/orders')}}" class="shipping">Show Orders</a></li>
              
          </ul>
      </li>
      
    <li><h3><a href="{{url('/admin/users')}}" class="user">Users</a></h3>
            <ul>
              <li><a href="{{url('/admin/user/create')}}" class="useradd">Create user</a></li>
              <li><a href="{{url('/admin/users')}}" class="group">Show Users</a></li>
              
          </ul>
      </li>

      <li><h3><a href="{{url('/admin/foods')}}" class="manage">Foods</a></h3>
            <ul>
              <li><a href="{{url('/admin/food/create')}}" class="manage_page">Create Food</a></li>
              <li><a href="{{url('/admin/foods')}}" class="cart">Show Foods</a></li>
              <!-- <li><a href="#" class="folder">Product categories</a></li>
              <li><a href="#" class="promotions">Promotions</a></li> -->
          </ul>
      </li>
  </ul>       
</div>
	

		<div id="wrapper">
            <div id="content">
                <div id="box">
                	<h3>Foods</h3>
                  <div id="show"></div>
                    <form id="myform">
                    <table id="table">
                         
                    </table>
                    <input type="submit" value="Update Food"/>
                    </form>

                
                	
                    
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

                    <script>	
                    $(document).ready((function(){
                    var $o = $('#table');
                    var a={{$foodid}};
                    var token=localStorage.getItem('token');
                    var order={
                        token:token ,
                      }
                    $.ajax({
                    type: 'POST',
                    url: '../../api/food',
                    dataType: 'json',
                    data: order,
                    success: function(foods){
           
                    $.each(foods.results.food, function(){
                        
                        if(this.foodid==a){
                        $o.append('<tr><th>'+"foodid"+'</th><td><input type="text" disabled="true"id="foodid" value="'+this.foodid+'"/></td></tr><tr><th>'
                                            +"name"+'</th><td><input type="text" id="name" value="'+this.name+'"/></td></tr><tr><th>'
                                            +"description"+'</th><td><input type="text" id="description" value="'+this.description+'"/></td></tr><tr><th>'
                                            +"L"+'</th><td><input type="text" id="L" value="'+this.L+'"/></td></tr><tr><th>'
                                            +"D"+'</th><td><input type="text" id="D" value="'+this.D+'"/></td></tr><tr><th>'
                                            +"starters"+'</th><td><input type="text" id="starters" value="'+this.starters+'"/></td></tr><tr><th>'
                                            +"thali"+'</th><td><input type="text" id="thali" value="'+this.thali+'"/></td></tr><tr><th>'
                                            +"chappati"+'</th><td><input type="text" id="chappati" value="'+this.chappati+'"/></td></tr><tr><th>'
                                            +"vegetable"+'</th><td><input type="text" id="vegetable" value="'+this.vegetable+'"/></td></tr><tr><th>'
                                            +"salad&pickle"+'</th><td><input type="text" id="saladNpickle" value="'+this.saladNpickle+'"/></td></tr><tr><th>'
                                            +"desert"+'</th><td><input type="text" id="desert" value="'+this.desert+'"/></td></tr><tr><th>'
                                            +"complementary"+'</th><td><input type="text" id="complementary" value="'+this.complementary+'"/></td></tr><tr><th>'
                                            +"dailyprice"+'</th><td><input type="text" id="dailyprice" value="'+this.dailyprice+'"/></td></tr><tr><th>'
                                            +"weeklyprice"+'</th><td><input type="text" id="weeklyprice" value="'+this.weeklyprice+'"/></td></tr><tr><th>'
                                            +"monthlyprice"+'</th><td><input type="text" id="monthlyprice" value="'+this.monthlyprice+'"/></td></tr>');
                        }
                        
                        
                     });
                    }
                    });
                    }));

              $(document).ready(function(){
                      $('form#myform').submit(function(event){
                        event.preventDefault();
                        var foodid=$('#foodid').val();
                        var name=$('#name').val();
                        var description=$('#description').val();
                        var L=$('#L').val();
                        var D=$('#D').val();
                        var starters=$('#starters').val();
                        var thali=$('#thali').val();
                        var chappati=$('#chappati').val();    
                        var vegetable=$('#vegetable').val();    
                        var salad=$('#saladNpickle').val();   
                        var desert=$('#desert').val();    
                        var complementary=$('#complementary').val();    
                        var dailyprice=$('#dailyprice').val();   
                        var weeklyprice=$('#weeklyprice').val();
                        var monthlyprice=$('#monthlyprice').val();   
                        var token=localStorage.getItem('token');
                    
                    var order={
                        foodid: foodid,
                        name: name,
                        description: description,
                        L: L,
                        D:D,
                        starters:starters,
                        thali:thali,
                        chappati:chappati,
                        vegetable:vegetable,
                        saladNpickle: salad,
                        desert:desert,
                        complementary: complementary,
                        dailyprice : dailyprice,
                        weeklyprice : weeklyprice,
                        monthlyprice : monthlyprice,
                        token:token,

                    };
    

            $.ajax({
                 type: 'POST',
                url: '../../api/updatefood',
                data: order,
                success: function(elm){

                if(elm.results.success==1){
                   alert('Successfully Updated!')
                    window.location="{{url('admin/foods')}}";
                    
            //alert('error');
            }
                else{
                   $('#show').append('<p id="para" style="color:red;" >'+elm.results.message+'</p>')
                }
            }
        });

            });  
        });
                    </script> 
                    
                
                <form id="myform1">
                    <input type="hidden" id="foodid" value={{$foodid}} />
                    <input type="submit" value="Delete food"/>

                </form>
                <script>
                $(document).ready(function(){
                      $('form#myform1').submit(function(event){
                        event.preventDefault();
                        var foodid=$('#foodid').val();
                         var token=localStorage.getItem('token');

                    var order={
                        foodid: foodid,
                        token:token,

                    };
    

            $.ajax({
                 type: 'POST',
                url: '../../api/deletefood',
                data: order,
                success: function(elm){
                  // console.log(elm);
                if(elm.results.success==1){
                    alert('Successfully Deleted!')
                    window.location="{{url('admin/foods')}}";
            }
                else{
                  $('#show').html('<p id="para" style="color:red;" >'+elm.results.message+'</p>')
            //alert('error');
                    
                }
            }
        });

        }); 

        $('#logout').on('click', function(){

              var token=localStorage.getItem('token');
              var order={
                  token:token,
              };
              $.ajax({
                  type: 'POST',
                  url: '../../api/adminlogout',
                  data: order,
                  success: function(elm){
                      if(elm.results.success){
                        localStorage.removeItem('token');
                        window.location="{{url('/admin')}}";
                      }
                      else{
                        alert('Internal server Error');
                      }
                  }
          });
              
      }); 
        });
                    </script> 