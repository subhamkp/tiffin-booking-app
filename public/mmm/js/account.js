window.onload = function(){
	checkLoginStatus().then(()=>{
		loadData();
		getBookings();
	}).catch(()=>{
		returnToHome();
	});	
};

function checkLoginStatus(){
	return new Promise((resolve, reject)=>{
		if(localStorage.getItem('loggedIn')!=null && localStorage.loggedIn == 1){
			resolve("logged in");
		}else{
			reject("not logged in");
		}
	})
}

function loadData(){
	var token = localStorage.token;
	var url = `http://18.220.195.13/api/userdetails?token=${token}`

	fetch(url, {
		method: 'POST',
		accept: 'application/json'
	}).then((res)=>(res.json())).then((res)=>{
		// console.log(res);
		document.getElementById('name').innerHTML = res.results.name;
		localStorage.setItem('name', res.results.name);
		document.getElementById('mobile').innerHTML = res.results.mobile;
		localStorage.setItem('mobile', res.results.mobile);
		document.getElementById('email').innerHTML = res.results.email;
		localStorage.setItem('email', res.results.email);
		document.getElementById('status').innerHTML = (res.results.isVerified && 'Verified')||('Not Verified');
		localStorage.setItem('status', (res.results.isVerified && 'Verified')||('Not Verified'));
		localStorage.setItem('userId', res.results.id);
	})
}

var mealNames = ['Veg Value Meal','Non Veg Value Meal','Veg Basic Meal','Non Veg Basic Meal','Veg Shahi Meal','Non Veg Shahi Meal'];

function getBookings(){
	var mobile = localStorage.mobile;
	var token = localStorage.token;
	var url = `http://18.220.195.13/api/viewbooking?usermobile=${mobile}&token=${token}`;
	fetch(url,{
		method: 'POST',
		accept: 'application/json'
	}).then((res)=>(res.json())).then((res)=>{
		var bookings = document.getElementById('bookings');
		var bookingsData = res.results.bookings;
		bookingsData.forEach((item) => {
			let mealTime;
			if(item.L==1 && item.D==1){
				mealTime = ' (D,L)';
			}else if(item.L==1 && item.D==0){
				mealTime = ' (L)';
			}else if(item.L==0 && item.D==1){
				mealTime = ' (D)';
			}
			let mealType = mealNames[item.typemeal] + mealTime;
			let newBooking = document.createElement('div');
			newBooking.className = "row booking";
			newBooking.innerHTML = `
				<div class="col-sm-4"><b>Booking ID:</b> ${item.bookingid}</div>
				<div class="col-sm-4"><b>Created At:</b> ${item.created_at}</div>
				<div class="col-sm-4"><b>Updated At:</b> ${item.updated_at}</div>
				<div class="col-sm-4"><b>Meal Type:</b> ${mealType}</div>
				<div class="col-sm-4"><b>Booking Option:</b> ${item.bookingoption}</div>
				<div class="col-sm-4"><b>No of Subscriptions:</b> ${item.no_of_subscriptions}</div>
				<div class="col-sm-4"><b>Delivery Address:</b> ${item.address}</div>
				<div class="col-sm-4"><b>Delivery Mobile:</b> ${item.deliverymobile}</div>
				<div class="col-sm-4"><b>Name:</b> ${item.name}</div>
				<div class="col-sm-4"><b>Starting Date:</b> ${item.starting_date}</div>
				<div class="col-sm-4"><b>Last Date:</b> ${item.last_date}</div>
			`;
			bookings.appendChild(newBooking);
		})
	})

}

function returnToHome(){
	window.open('index.html','_self');
}