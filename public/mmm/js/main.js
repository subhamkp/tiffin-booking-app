var i; //iterator
var meal = 1; //default plan

window.onload = function(){
	planDetails(0);
	addFunctionalityToDpIcon();
	changeFeatured();
	checkLoginStatus();
}

function checkLoginStatus(){
	if(localStorage.getItem('loggedIn')!=null){
		if(localStorage.loggedIn == '1'){
			isLoggedIn();
			document.getElementById('header-user-name').innerHTML = localStorage.name || "";
		}else{
			isNotLoggedIn();
		}
	}else{
		isNotLoggedIn();
	}
}

var notLoggedInElements = document.getElementsByClassName('not-logged-in');
var loggedInElements = document.getElementsByClassName('logged-in');
var registerElements = document.getElementsByClassName('registering');
var verifyElements = document.getElementsByClassName('verifying');

function isLoggedIn(){
	for(i=0; i<notLoggedInElements.length; i++){
		notLoggedInElements[i].style.display = 'none';
	}
	for(i=0; i<registerElements.length; i++){
		registerElements[i].style.display = 'none';
	}
	for(i=0; i<verifyElements.length; i++){
		verifyElements[i].style.display = 'none';
	}
	for(i=0; i<loggedInElements.length; i++){
		loggedInElements[i].style.display = 'inline-block';
	}
}

function isRegistering(){
	for(i=0; i<notLoggedInElements.length; i++){
		notLoggedInElements[i].style.display = 'none';
	}
	for(i=0; i<registerElements.length; i++){
		registerElements[i].style.display = 'inline-block';
	}
	for(i=0; i<verifyElements.length; i++){
		verifyElements[i].style.display = 'none';
	}
	for(i=0; i<loggedInElements.length; i++){
		loggedInElements[i].style.display = 'none';
	}
}

function isVerifying(){
	for(i=0; i<notLoggedInElements.length; i++){
		notLoggedInElements[i].style.display = 'none';
	}
	for(i=0; i<registerElements.length; i++){
		registerElements[i].style.display = 'none';
	}
	for(i=0; i<verifyElements.length; i++){
		verifyElements[i].style.display = 'inline-block';
	}
	for(i=0; i<loggedInElements.length; i++){
		loggedInElements[i].style.display = 'none';
	}
}

function isNotLoggedIn(){
	for(i=0; i<notLoggedInElements.length; i++){
		notLoggedInElements[i].style.display = 'inline-block';
	}
	for(i=0; i<registerElements.length; i++){
		registerElements[i].style.display = 'none';
	}
	for(i=0; i<verifyElements.length; i++){
		verifyElements[i].style.display = 'none';
	}
	for(i=0; i<loggedInElements.length; i++){
		loggedInElements[i].style.display = 'none';
	}
}

document.getElementById('login').addEventListener('click', attemptLogin);

function attemptLogin(){
	var mobile = document.getElementById('usermobile').value;
	var password = document.getElementById('userpassword').value;

	var url = `http://18.220.195.13/api/login?mobile=${mobile}&password=${password}`;

	fetch(url, {
		method: 'POST',
		accept: 'application/json'
	}).then((res)=>(res.json())).then((res)=>{
		if(res.results.success == 1){
			document.getElementById('usermobile').value = null;
			document.getElementById('userpassword').value = null;
			localStorage.setItem('loggedIn', 1);
			localStorage.setItem('token', res.results.token);
			localStorage.setItem('mobile', mobile);
			displayToast("You have successfully logged in.", 1);
			loadData();
			checkLoginStatus();
		}else{
			if(res.results.errorcode==3){
				document.getElementById('userpassword').value = null;
				displayToast("Password Wrong. Try Again.", 0);
			}else if(res.results.errorcode==1){
				isRegistering();
				displayToast("Enter your Name and Email to register", 1);
			}else if(res.results.errorcode==2){
				displayToast("You need to verify OTP before logging in.", 0);
			}
		}
	});
}

document.getElementById('register').addEventListener('click', attemptRegister);

function attemptRegister(){
	var mobile = document.getElementById('usermobile').value;
	var password = document.getElementById('userpassword').value;
	var name = document.getElementById('username').value;
	var email = document.getElementById('useremail').value;

	var url = `http://18.220.195.13/api/register?mobile=${mobile}&password=${password}&name=${name}&email=${email}`;

	fetch(url, {
		method: 'POST',
		accept: 'application/json'
	}).then((res)=>(res.json())).then((res)=>{
		isVerifying();
	});
}

document.getElementById('verify').addEventListener('click', attemptVerify);

function attemptVerify(){
	var mobile = document.getElementById('usermobile').value;
	var otp = document.getElementById('userotp').value;

	var url = `http://18.220.195.13/api/verifyotp?mobile=${mobile}&otp=${otp}`;

	fetch(url, {
		method: 'POST',
		accept: 'application/json'
	}).then((res)=>(res.json())).then((res)=>{
		console.log(res);
	});
}

function loadData(){
	var token = localStorage.token;
	var url = `http://18.220.195.13/api/userdetails?token=${token}`

	fetch(url, {
		method: 'POST',
		accept: 'application/json'
	}).then((res)=>(res.json())).then((res)=>{
		localStorage.setItem('name', res.results.name);
		document.getElementById('header-user-name').innerHTML = res.results.name;
		localStorage.setItem('userId', res.results.id);
	})
}

document.getElementById('profile').addEventListener('click', showProfile);

function showProfile(){
	window.open('account.html', '_self');
}

document.getElementById('logout').addEventListener('click', attemptLogout);

function attemptLogout(){
	var token = localStorage.token;
	var url = `http://18.220.195.13/api/logout?token=${token}`;
	// var url = `http://10.10.146.171/tiffinapp/public/api/logout?token=${token}`;

	fetch(url, {
		method: 'POST',
		accept: 'application/json'
	}).then((res)=>(res.json())).then((res)=>{
		if(res.results.success==1){
			localStorage.clear();
			checkLoginStatus();
		}
	});
}

function addFunctionalityToDpIcon(){
	var headerUserDp = document.getElementById("header-user-dp");
	var headerUser = document.getElementById("header-user");
	headerUserDp.addEventListener("click", () => {
		if(headerUser.className == "expand"){
			headerUser.className = "";
		}else{
			headerUser.className = "expand"
		}
	});
}

var fimages = document.getElementsByClassName('featured-dish-image');
var fdesc = document.getElementsByClassName('fdesc');
var ftit = document.getElementsByClassName('ftit');

var active = 0;

function changeFeatured(){

	for(var i=0; i<fimages.length; i++){
		if(i!=active){
			fimages[i].style.display = "none";
			fdesc[i].style.display = "none";
			ftit[i].style.display = "none";
		}else{
			fimages[i].style.display = "block";
			fdesc[i].style.display = "block";
			ftit[i].style.display = "block";
		}
	}
	active = (active+1)%fimages.length;

	setTimeout(changeFeatured, 2500);
}

var pcd = document.getElementsByClassName('pcd');
var pci = document.getElementsByClassName('plans-images-image');
var pll = document.getElementsByClassName('pll');

function planDetails(n){
	meal = n+1;
	for(var i=0; i<6; i++){
		if(i==n){
			pcd[i].style.display = "block";
			pll[i].className = "pll active";
			for(var j=i*4; j<i*4+4; j++){
				pci[j].style.display = "block";
			}
		}else{
			pcd[i].style.display = "none";
			pll[i].className = "pll";
			for(var j=i*4; j<i*4+4; j++){
				pci[j].style.display = "none";
			}
		}
	}
}

document.getElementById('book-now').addEventListener('click', showBookingModal);

function showBookingModal(){
	document.getElementById('booking-modal').style.display = "block";
}

document.getElementById('hide-booking').addEventListener('click', hideBookingModal);

function hideBookingModal(){
	document.getElementById('booking-modal').style.display = "none";
}

document.getElementById('booking-modal-button').addEventListener('click', attemptBooking);

function attemptBooking(){

	var token = localStorage.token;
	var name = localStorage.name;
	var mobile = localStorage.mobile;
	var dinner = (document.getElementById('dinner').checked && 1) || 0;
	var lunch = (document.getElementById('lunch').checked && 1) || 0;
	var deliveryPhone = document.getElementById('delivery-phone').value.substr(-10, 10);
	var deliveryAddress = document.getElementById('delivery-address').value;
	var bookingOption = document.getElementById('booking-option').value;
	var startingDate = document.getElementById('starting-date').value;
	var quantity = document.getElementById('quantity').value;

	var url = `http://18.220.195.13/api/createbooking?
	token=${token}&L=${lunch}&D=${dinner}&name=${name}&address=${deliveryAddress}
	&usermobile=${mobile}&deliverymobile=${deliveryPhone}&bookingoption=${bookingOption}
	&typemeal=${meal}&starting_date=${startingDate}&no_of_subscriptions=${quantity}`;

	fetch(url, {
		method: 'POST',
		accept: 'applicatin/json'
	}).then((res)=>(res.json())).then((res)=>{
		if(res.results.success == 1){
			hideBookingModal();
			displayToast("Your booking has been successfully created.", 1);
		}
	})
}

var toast = document.getElementById('toast');
var toastContent = document.getElementById('toast-content');
var toastTimeout = null;

function displayToast(text, bool){

	console.log("displayToast");

	clearTimeout(toastTimeout);

	if(bool==1){
		toast.style.backgroundColor = "#7ee09e" 
	}else if(bool==0){
		toast.style.backgroundColor = "#e27d7d"
	}

	toastContent.innerHTML = text;

	toast.style.display = "inline-block";

	toastTimeout = setTimeout(() => {
		toast.style.display = "none";
	}, 2500);

}