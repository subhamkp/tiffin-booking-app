<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\User;
use Illuminate\Http\Request;
use App\authacesstoken;
use LaravelMsg91;
use App\Helpers\Manager;
use DB;

class usercontroller extends Controller
{
    public $successStatus = 200;

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'mobile'=>'required'
        ]);

        if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg], 400);           
        }

        $input = $request->all();

        if($user=DB::table('users')->where('mobile',$input['mobile'])->first())
        {
            if($user->isVerified==0){
                $msg['message']="your number is already registered please verify your account to login";

                 //create OTP
                $manager=new Manager;
                $otp=$manager->generate($input['mobile'], $user->id);

                // $message="Hello ".$user->name.", ".$otp->token." is your OTP for TIFFIN APP verification. It is valid for 10 minutes.";

                //send OTP
                //$result = LaravelMsg91::message($input['mobile'], $message);
                $result="message not sent try";

                //response in api
                $msg['otp']=$otp->token;
                $msg['userid']=$user->id;
                $msg['mobile']=$otp->mobile;
                $msg['isVerified']=$user->isVerified;
                $msg['msg_status']=$result;
                $msg['success']="0";
                $msg['error']="1";
                $msg['errorcode']="4";

                return response()->json(['results' => $msg]);

            }
            else{
                $msg['message']="your number is already registered, please login";
                $msg['error']=1;
                $msg['success']="0";
                $msg['errorcode']="5";
                return response()->json(['results' => $msg]);
            }
            
        }

        $input['password'] = bcrypt($input['password']);
        $input['isVerified']=0;

        //create user 
        $user = User::create($input);

        //create OTP
        $manager=new Manager;
        $otp=$manager->generate($input['mobile'], $user->id);

        // $message="Hello ".$user->name.", ".$otp->token." is your OTP for TIFFIN APP verification. It is valid for 10 minutes.";
        
        //send OTP
        //$result = LaravelMsg91::message($input['mobile'], $message);
        $result="message not sent try";
        
        //response in api
        $msg['message']="you have been successfully registered please verify your account";
        $msg['otp']=$otp->token;
        $msg['userid']=$user->id;
        $msg['mobile']=$otp->mobile;
        $msg['isVerified']=$user->isVerified;
        $msg['msg_status']=$result;
        $msg['success']="1";
        $msg['error']="0";

        return response()->json(['results' => $msg]);

    }

    /**
    * Function for Login.
    *
    * @return Response
    */
    public function login(Request $request){
       
        $mobile = $request->input('mobile');

        if(!User::where('mobile',$mobile)->first())
            {
                $msg['success']='0';
                $msg['message']='Mobile no is not registered';
                $msg['error']="1";
                $msg['errorcode']="2";
                return response()->json(['results' => $msg], 400);
            }
        $password = $request->input('password');

        

        if (Auth::attempt([ 'mobile'=> $mobile, 'password'  => $password ])) {
            $user = Auth::user();

            $status=$user->where(['isVerified'=>'1','mobile'=>$mobile])->first();

            if($status){
                $access =  $user->createToken('Tiffinapp');

                
                $data=array();

                $data['user_id']=$access->token->user_id;
                $data['token_id']=$access->token->id;
                $data['mobile']=$user->mobile;
                $data['token']=$access->accessToken;
                $data['success']='1';
                
                authacesstoken::create($data);
                $success['token']=$access->accessToken;
                $success['success']="1";
                $success['error']="0";
                $success['message']="Use the token for further queries";
                return response()->json(['results' => $success], $this->successStatus);
            }
            else
            {
                $msg['success']='0';
                $msg['message']='Please login after verifying your account';
                $msg['error']="1";
                $msg['errorcode']="3";
                return response()->json(['results' => $msg], 400);
            }
        }
        else{
            $msg['success']='0';
            $msg['message']='please input your correct password again';
            $msg['error']="1";
            $msg['errorcode']="1";
            return response()->json(['results'=>$msg], 401);
        }
    }


    /*
        logout api
    */    
    
    public function logout(Request $request)
    { 
       $token=$request->input('token');

       $usersession1 = DB::table('authacesstokens')->where('token',$token);

       $useraccess=$usersession1->first();
        if(!($useraccess))
            {
                $result['message']='Enter valid token';
                $result['error']="1";
                $result['success']='0';
                return response()->json(['results'=>$result]);
            }

       $token_id=$usersession1->first()->token_id;
       $usersession1->delete();
        $usersession2=DB::table('oauth_access_tokens')->where('id',$token_id);
        $usersession2->delete();
        
         $say['message']='You have been sucessfully logged out';
         $say['success']='1';
         $say['error']="0";
         return response()->json(['results'=>$say],$this->successStatus);
    }

    /*
       for forget Password we use getotp in msg controller
    */


    /*
        Reset Password
    */
    public function resetpassword(Request $request)
    {

         $validator = Validator::make($request->all(), [
            'otp' => 'required',
            'password' => 'required',
            'mobile'=>'required'
        ]);

        if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg], 400);           
        }

        $mobile=$request->input('mobile');
        $newpassword=$request->input('password');
        $otp = $request->input('otp');

        $user = User::where('mobile',$mobile)->first();
        $userid=$user->id;

        $manager=new Manager;
        $result=$manager->isValid($otp,$mobile,$userid);

        if($result)
        {
            $newpassword= bcrypt($newpassword);

            User::where('id', $userid)->update(['password' => $newpassword]);
            $response['success'] = 1;
            $response['message'] = "Your Password has been reset.";
            $response['error']="0";
        }
        else
        {
            $response['success'] =0;
            $response['message'] = "OTP does not match try resend otp";
            $response['error'] = 1;
        }
        return response()->json(['results'=>$response],400);
    }

    public function  userdetails(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'token'=>'required',
        ]);


        if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg], 400);           
        }
        
        $token=$request->input('token');

        $useraccess=DB::table('authacesstokens')->where('token',$token)->first();
        if(!($useraccess))
            {
                $result['error']='Enter valid token';
                $result['success']='0';
                return response()->json(['results'=>$result]);
            }
        $usermobile=$useraccess->mobile;
        $user=DB::table('users')->where('mobile',$usermobile)->first();

        if(DB::table('users')->where('mobile',$usermobile)->first()){
            $result['id']=$user->id;
            $result['name']=$user->name;
            $result['mobile']=$user->mobile;
            $result['isVerified']=$user->isVerified;
            $result['email']=$user->email;
            $result['success']="1";
            return response()->json(['results'=>$result]);
        }
        else{
            $result['error']='Internal Server Error';
            $result['success']="0";
            return response()->json(['results'=>$result]);
        }

    }
}
