<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\booking;
use App\User;
use Illuminate\Http\Request;
use App\authacesstoken;
use DB;


class foodcontroller extends Controller
{
    public function food(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'token'=>'required',
            ]);

            if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg], 400);           
            }
    
           $token=$request->input('token');

           $useraccess=DB::table('authacesstokens')->where('token',$token)->first();
           $adminaccess=DB::table('adminaccesstoken')->where('token',$token)->first();
           if(!($useraccess||$adminaccess))
            {
                $result['success']="0";
                $result['error']="1";
                $result['message']="Enter valid token";
                return response()->json(['results'=>$result]);
            }
            

            $food=DB::table('foods')->get();
            $result['success']="1";
            $result['error']="0";
            $result['food']=$food;
            return response()->json(['results'=>$result]);
    }

    public function lunch(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'token'=>'required',
            ]);

            if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg], 400);           
            }
    
           $token=$request->input('token');

           $useraccess=DB::table('authacesstokens')->where('token',$token)->first();
           $adminaccess=DB::table('adminaccesstoken')->where('token',$token)->first();
           if(!($useraccess||$adminaccess))
            {
                $result['success']="0";
                $result['error']="1";
                $result['message']="Enter valid token";
                return response()->json(['results'=>$result]);
            }
    	$food=DB::table('foods')->where('L','1')->get();
        $result['success']="1";
        $result['error']="0";
        $result['lunch']=$food;
        return response()->json(['results'=>$result]);
    }
    
    public function dinner(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'token'=>'required',
            ]);

            if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg], 400);           
            }
    
           $token=$request->input('token');

           $useraccess=DB::table('authacesstokens')->where('token',$token)->first();
           $adminaccess=DB::table('adminaccesstoken')->where('token',$token)->first();
           if(!($useraccess||$adminaccess))
            {
                $result['success']="0";
                $result['error']="1";
                $result['message']="Enter valid token";
                return response()->json(['results'=>$result]);
            }
        $food=DB::table('foods')->where('D','1')->get();
        $result['success']="1";
        $result['error']="0";
        $result['dinner']=$food;
    	return response()->json(['results'=>$result]);
    }

    //booking option
    // public function bookingoption()
    // {
    //     $bookingoption=DB::table('booking_option')->get();
    // 	return response()->json(['results'=>$bookingoption]);
    // }

    //create booking
    public function  createbooking(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'token'=>'required',
            'L' => 'required',
            'D' => 'required',
            'name' => 'required',
            'address' => 'required',
            'usermobile' => 'required',
            'deliverymobile' => 'required',
            'bookingoption' => 'required',
            'typemeal' => 'required',
            'starting_date'=>'required',
            'no_of_subscriptions'=>'required',
        ]);


        if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg], 400);           
        }
        
        $input = $request->all();
        $token=$request->input('token');

        $useraccess=DB::table('authacesstokens')->where('token',$token)->first();
        if(!($useraccess))
            {
                $result['message']='Enter valid token';
                $result['error']="1";
                $result['success']='0';
                return response()->json(['results'=>$result]);
            }

        if(!DB::table('foods')->where('foodid',$input['typemeal'])->first())
            {
                $result['message']='Enter correct meal id';
                $request['error']="1";
                $result['success']="0";
                return response()->json(['results'=>$result]);
            }
        if(!DB::table('users')->where('mobile',$input['usermobile'])->first())
            {
                $result['message']='Enter correct user mobile';
                $result['error']="1";
                $result['success']='0';
                return response()->json(['results'=>$result]);
            }

        //create booking 
        $createbooking = booking::create($input);
        //getting user details
        $booking=DB::table('bookings')->where('usermobile',$createbooking->usermobile)->orderBy('created_at','desc')->first();
        $bookingid=$booking->bookingid;
        $usermobile=$createbooking->usermobile;
        $bookingoption=$createbooking->bookingoption;
        $typemeal=$request->input('typemeal');
        $user=DB::table('users')->where('mobile',$usermobile)->first();
        $curr_date=$createbooking->starting_date;

        if($createbooking->bookingoption=="weekly"){
            $lastdate=strtotime("+6 days",strtotime($curr_date));
            $lastdate=date('Y-m-d',$lastdate);
            DB::table('bookings')->where('bookingid',$bookingid)->update(["last_date"=>$lastdate]);
        }

        if($createbooking->bookingoption=="monthly"){
            $lastdate=strtotime("+29 days",strtotime($curr_date));
            $lastdate=date('Y-m-d',$lastdate);
            DB::table('bookings')->where('bookingid',$bookingid)->update(["last_date"=>$lastdate]);
        }

        $charges=DB::table('foods')->where('foodid',$typemeal)->first()->dailyprice;
        $quantity=$createbooking->no_of_subscriptions;
        
        //weekly
        if($createbooking->bookingoption=="weekly")
        {
            if($createbooking->L){
               
                for($i=0;$i<7;$i++){
                    for($J=0;$J<$quantity;$J++){
                    DB::table('tiffin')->insert(array(
                    array("L"=>"$createbooking->L",
                      "D"=>"0",
                      "bookingid"=>"$bookingid",
                      "customerid"=>"$user->id",
                      "bookingoption"=>"$createbooking->bookingoption",
                      "date"=>"$curr_date",
                      "status"=>"pending",
                      "typemeal"=>"$typemeal",
                      "charges"=>$charges
                      )
                    )
                    );
                  }
                    $curr_date=date('Y-m-d', strtotime('+1 day', strtotime($curr_date)));
                }
            }

            $curr_date=$createbooking->starting_date;

            if($createbooking->D){
                for($i=0;$i<7;$i++){
                  for($J=0;$J<$quantity;$J++){
                    DB::table('tiffin')->insert(array(
                    array("L"=>"0",
                      "D"=>"$createbooking->D",
                      "bookingid"=>"$bookingid",
                      "customerid"=>"$user->id",
                      "bookingoption"=>"$createbooking->bookingoption",
                      "date"=>"$curr_date",
                      "status"=>"pending",
                      "typemeal"=>"$typemeal",
                      "charges"=>$charges
                      )
                    )
                    );
                  }  
                    $curr_date=date('Y-m-d', strtotime('+1 day', strtotime($curr_date)));
               }
            }

        }
            //monthly
            if($createbooking->bookingoption=="monthly")
            {
            if($createbooking->L){
                for($i=0;$i<30;$i++){
                  for($J=0;$J<$quantity;$J++){
                    DB::table('tiffin')->insert(array(
                    array("L"=>"$createbooking->L",
                    "D"=>"0",
                    "bookingid"=>"$bookingid",
                    "customerid"=>"$user->id",
                    "bookingoption"=>"$createbooking->bookingoption",
                    "date"=>"$curr_date",
                    "status"=>"pending",
                    "typemeal"=>"$typemeal",
                    "charges"=>$charges
                    )
                    )
                );
              }
                    $curr_date=date('Y-m-d', strtotime('+1 day', strtotime($curr_date)));
                }
            }

            $curr_date=$createbooking->starting_date;

            if($createbooking->D){

                for($i=0;$i<30;$i++){

                  for($J=0;$J<$quantity;$J++){
                    DB::table('tiffin')->insert(array(
                    array("L"=>"0",
                      "D"=>"$createbooking->D",
                      "bookingid"=>"$bookingid",
                      "customerid"=>"$user->id",
                      "bookingoption"=>"$createbooking->bookingoption",
                      "date"=>"$curr_date",
                      "status"=>"pending",
                      "typemeal"=>"$typemeal",
                      "charges"=>$charges
                      )
                    )
                    );
                 }

                    $curr_date=date('Y-m-d', strtotime('+1 day', strtotime($curr_date)));
               }
            }

        }

         if($orderdetails=DB::table('bookings')->where('bookingid',$bookingid)->first()){
            $result['message']='Order has been created';
            $result['success']='1';
            $result['error']='0';
            return response()->json(['results'=>$result]);
        }
        else{
            $result['message']='Try again, something wrong happened';
            $result['error']="1";
            $result['success']='0';
            return response()->json(['results'=>$result]);        
        }

       }
        //viewbooking

        public function viewbooking(Request $request)
        {  
             //validate data
            $validator = Validator::make($request->all(), [
                'token'=>'required',
                'usermobile'=>'required',
            ]);
            if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg], 400);           
            }
           
           $usermobile=$request->input('usermobile');
           $token=$request->input('token');

           $useraccess=DB::table('authacesstokens')->where('token',$token)->first();
           if(!$useraccess)
            {
                $result['success']="0";
                $request['error']="1";
                $result['message']="Enter valid token";
                return response()->json(['results'=>$result]);
            }
            if(!DB::table('users')->where('mobile',$usermobile)->first())
            {
                $result['success']="0";
                $request['error']="1";
                $result['message']="Enter correct usermobile";
                return response()->json(['results'=>$result]);
            }

           $viewbooking=DB::table('bookings')->where('usermobile',$usermobile)->get();

           $result['success']="1";
           $result['error']="0";
           $result['message']="Check the booking of the user";
           $result['bookings']=$viewbooking;
           return response()->json(['results'=>$result]);
        }


        //viewtiffins
        public function viewtiffins(Request $request)
        {
             //validate data
            $validator = Validator::make($request->all(), [
                'token'=>'required',
                'bookingid'=>'required',
            ]);

            if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg], 400);           
             }

            $token=Request('token');
            $bookingid = Request('bookingid');
           //check access
           
           $useraccess=DB::table('authacesstokens')->where('token',$token)->first();
           if(!$useraccess)
            {
                $result['success']="0";
                $request['error']-"1";
                $result['message']="Enter valid token";
                return response()->json(['results'=>$result]);
            }

            $viewtiffins=DB::table('tiffin')->where('bookingid',$bookingid)->get();
            $result['success']="1";
            $result['error']="0";
            $result['tiffin']=$viewtiffins;
            return response()->json(['results'=>$result]);
        }


        //updatetiffins
        public function updatetiffin(Request $request)
        {
             //validate data
            $validator = Validator::make($request->all(), [
                'token'=>'required',
                'bookingid'=>'required',
                'tiffinid'=>'required',
                'date'=>'required',
                'typemeal'=>'required',
            ]);

            if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg], 400);           
            }
            
            $token=$request->input('token');
            $bookingid=$request->input('bookingid');
            $tiffinid=$request->input('tiffinid');
            $updatedate=$request->input('date');
            $updatemeal=$request->input('typemeal');
           //check access
           
            $useraccess=DB::table('authacesstokens')->where('token',$token)->first();
            $adminaccess=DB::table('adminaccesstoken')->where('token',$token)->first();
            if(!($useraccess||$adminaccess))
            {
                $result['success']="0";
                $result['error']="1";
                $result['message']="Enter valid token";
                return response()->json(['results'=>$result]);
            }

            if(!DB::table('foods')->where('foodid',$updatemeal)->first())
            {
                $result['success']="0";
                $result['error']="1";
                $result['message']="Enter correct meal id";
                return response()->json(['results'=>$result]);
            }
            
            $tiffin=DB::table('tiffin')->where('bookingid',$bookingid)->where('tiffinid',$tiffinid)->first();
            $forlastdate=DB::table('bookings')->where('bookingid',$bookingid)->first();

            $currentdate=Carbon\Carbon::now();
            $lastdate=$forlastdate->last_date;
            $lastdatenew=strtotime("+3 days",strtotime($lastdate));
            $lastdatenew=date('Y-m-d',$lastdatenew);
            
        //update date
            if($updatedate>=$currentdate && $updatedate<=$lastdatenew){
                DB::table('tiffin')->where('bookingid',$bookingid)->where('tiffinid',$tiffinid)->update(["date"=>$updatedate]);
                }       
            
            
            else{
                $result['success']="0";
                $result['error']="1";
                $result['message']="Enter a valid date between ".$currentdate." and ".$lastdatenew;
                return response()->json(['results'=>$result]);
            }
            
            
            $food=DB::table('foods')->where('foodid',$updatemeal)->first();
            $t=DB::table('tiffin')->where('bookingid',$bookingid)->where('tiffinid',$tiffinid);
            $t->update(["typemeal"=>$updatemeal]);
            $t->update(["charges"=>($food->dailyprice)*($t->first()->quantity)]);
            
            if($t->first()){
                $result['success']="1";
                $result['error']="0";
                $result['message']="Tiffin has been updated";
                return response()->json(['results'=>$result]);  
            }
            else{
                $result['success']="0";
                $result['error']="1";
                $result['message']="Try agian, something wrong happened";
                return response()->json(['results'=>$result]);            
        }
    }
        
}