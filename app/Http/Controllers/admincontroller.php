<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Food;
use App\User;
use App\booking;
use Illuminate\Http\Request;
use App\authacesstoken;
use DB;
use Carbon;
use Crypt;


class admincontroller extends Controller
{
    public function verifyadmin(Request $request){

           //validate data
           $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

        $username=$request->input('username');
        $password=$request->input('password');

        $a=Carbon\Carbon::now();
        $b=date(strtotime('+3 months +3 day', strtotime($a)));
        

        if($userdata=DB::table('adminlogin')->where('username',$username)->first()){
            if(Crypt::decrypt($userdata->password)==$password)
            {
                $token=Crypt::encrypt($username.$a.$password.$b);
                DB::table('adminaccesstoken')->insert(['token'=>$token,'date'=>$a]);
                $result['token']=$token;
                $result['error']="0";
                $result['success']="1";
                $result['message']="Use the token for further query";
                return response()->json(['results'=>$result]);  
            }
            else{
            	$result['message']="Enter correct password";
                $result['error']="1";
                $result['success']="0";
                return response()->json(['results'=>$result]);            
            }
        }
        else{
            $result['success']="0";
            $result['message']="Enter correct username";
            $result['error']="1";
            return response()->json(['results'=>$result]); 
        }

    }

    public function adminlogout(Request $request)
    {
         $validator = Validator::make($request->all(), [
            'token'=>'required',
        ]);

        if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

        if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
        {
            $result['message']='Enter valid token';
            $result['error']='1';
            $result['success']="0";
            return response()->json(['results'=>$result]);
        }

        if(DB::table('adminaccesstoken')->where('token',Request('token'))->delete())
            {
                $result['message']='Token has been deleted';
                $result['success']="1";
                $result['error']="0";
                return response()->json(['results'=>$result]);
            }

    }


    public function showbookings(Request $request){
         //validate data
        $validator = Validator::make($request->all(), [
            'token'=>'required',
        ]);

        if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

        if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
        {
            $result['error']='1';
            $result['message']='Enter valid token';
            $result['success']="0";
            return response()->json(['results'=>$result]);
        }

        $bookings=DB::table('bookings')->orderBy('bookingid', 'desc')->get();

        $result['error']='0';
        $result['message']='Check all the bookings';
        $result['success']="1";
        $result['bookings']=$bookings;
        return response()->json(['results'=>$result]);
    }

    public function showtiffins(Request $request){
        //validate data
        $validator = Validator::make($request->all(), [
            'token'=>'required',
        ]);

        if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

        if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
        {
        	$result['error']='1';
            $result['message']='Enter valid token';
            $result['success']="0";
            return response()->json(['results'=>$result]);
        }

        $tiffins=DB::table('tiffin')->get();

        $result['message']='Check all the tiffins';
        $result['success']="1";
        $result['tiffins']=$tiffins;
        return response()->json(['results'=>$result]);

    }


    public function showusers(Request $request){
        //validate data
        $validator = Validator::make($request->all(), [
            'token'=>'required',
        ]);

        if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

        if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
        {
        	$result['error']='1';
            $result['message']='Enter valid token';
            $result['success']="0";
            return response()->json(['results'=>$result]);
        }
        $users=DB::table('users')->get();
        
        $result['error']='0';
        $result['message']='show all users';
        $result['success']="1";
        $result['users']=$users;
        return response()->json(['results'=>$result]);
    }

    public function showuser(Request $request){
        //validate data
        $validator = Validator::make($request->all(), [
            'token'=>'required',
            'id'=>'required',
        ]);

        if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

        if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
        {
        	$result['error']='1';
            $result['message']='Enter valid token';
            $result['success']="0";
            return response()->json(['results'=>$result]);
        }

        $id=$request->input('id');
        $users=DB::table('users')->where('id',$id)->get();

        $result['error']='0';
        $result['message']='check the user ';
        $result['success']="1";
        $result['users']=$users;
        return response()->json(['results'=>$result]);
    }


    public function createfood(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'L' => 'required',
            'D' => 'required',
            'dailyprice'=>'required',
            'weeklyprice' => 'required',
            'monthlyprice' => 'required',
            'token'=>'required',
        ]);

        if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

        if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
        {
        	$result['error']='1';
            $result['message']='Enter valid token';
            $result['success']="0";
            return response()->json(['results'=>$result]);
        }

        $input = $request->all();

        //create user 
        $food = food::create($input);
        $name= $food->name;

        $starters=$request->input('starters');
        $thali=$request->input('thali');
        $chapppati=$request->input('chappati');
        $vegitable=$request->input('vegitable');
        $saladNpickle=$request->input('saladNpickle');
        $desert=$request->input('desert');
        $complementary=$request->input('complementary');
        DB::table('foods')->where('name',$name)->update(["starters"=>$starters]);
        DB::table('foods')->where('name',$name)->update(["thali"=>$thali]);
        DB::table('foods')->where('name',$name)->update(["chappati"=>$chapppati]);
        DB::table('foods')->where('name',$name)->update(["vegetable"=>$vegitable]);
        DB::table('foods')->where('name',$name)->update(["saladNpickle"=>$saladNpickle]);
        DB::table('foods')->where('name',$name)->update(["desert"=>$desert]);
        DB::table('foods')->where('name',$name)->update(["complementary"=>$complementary]);
        
        
        if($fooddetails=DB::table('foods')->where('name',$name)->first()){
        	$result['error']='0';
            $result['success']="1";
            $result['message']="Food has been created";
            return response()->json(['results'=>$result]);  
            }
            else{
            	$result['error']='1';
            	$result['error']='Try Again, something wrong happen';
	            $result['success']="0";
	            return response()->json(['results'=>$result]);             
            }
        }
        

    

        public function updatefood(Request $request)
        {
            //validate data
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'L' => 'required',
            'D' => 'required',
            'dailyprice'=>'required',
            'weeklyprice' => 'required',
            'monthlyprice' => 'required',
            'token'=>'required',
        ]);

        if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

        if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
        {
        	$result['error']='1';
            $result['message']='Enter valid token';
            $result['success']="0";
            return response()->json(['results'=>$result]);
        }

            $foodid=$request->input('foodid');
            $name=$request->input('name');
            $description=$request->input('description');
            $L=$request->input('L');
            $D=$request->input('D');
            $starters=$request->input('starters');
            $thali=$request->input('thali');
            $chapppati=$request->input('chappati');
            $vegetable=$request->input('vegetable');
            $saladNpickle=$request->input('saladNpickle');
            $desert=$request->input('desert');
            $complementary=$request->input('complementary');
            $dailyprice=$request->input('dailyprice');
            $weeklyprice=$request->input('weeklyprice');
            $monthlyprice=$request->input('monthlyprice');

            $food=DB::table('foods')->where('foodid',$foodid)->first();
            
        
        DB::table('foods')->where('foodid',$foodid)->update(["name"=>$name]);
        DB::table('foods')->where('foodid',$foodid)->update(["description"=>$description]);
        DB::table('foods')->where('foodid',$foodid)->update(["L"=>$L]);
        DB::table('foods')->where('foodid',$foodid)->update(["D"=>$D]);
        DB::table('foods')->where('foodid',$foodid)->update(["starters"=>$starters]);
        DB::table('foods')->where('foodid',$foodid)->update(["thali"=>$thali]);
        DB::table('foods')->where('foodid',$foodid)->update(["chappati"=>$chapppati]);
        DB::table('foods')->where('foodid',$foodid)->update(["vegetable"=>$vegetable]);
        DB::table('foods')->where('foodid',$foodid)->update(["saladNpickle"=>$saladNpickle]);
        DB::table('foods')->where('foodid',$foodid)->update(["desert"=>$desert]);
        DB::table('foods')->where('foodid',$foodid)->update(["complementary"=>$complementary]);
        DB::table('foods')->where('foodid',$foodid)->update(["dailyprice"=>$dailyprice]);
        DB::table('foods')->where('foodid',$foodid)->update(["weeklyprice"=>$weeklyprice]);
        DB::table('foods')->where('foodid',$foodid)->update(["monthlyprice"=>$monthlyprice]);
        

        if($fooddetails=DB::table('foods')->where('foodid',$foodid)->first()){
        	$result['error']='0';
            $result['success']="1";
            $result['messsage']='Food has been updated';
            return response()->json(['results'=>$result]);  
        }
        else{
        	$result['error']='1';
        	$result['message']='Try Again, something wrong happen';
            $result['success']="0";
            return response()->json(['results'=>$result]);           
        }
    
        
    }



    //updatetiffins
    public function updateuser(Request $request)
    {
        //validate data
    $validator = Validator::make($request->all(), [
        'id'=>'required',
        'name' => 'required',
        'email' => 'required',
        'mobile' => 'required',
        'token'=>'required',
    ]);

    if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

    if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
    {
    	$result['error']='1';
        $result['message']='Enter valid token';
        $result['success']="0";
        return response()->json(['results'=>$result]);
    }


    $id=$request->input('id');
    $name=$request->input('name');
    $email=$request->input('email');
    $mobile=$request->input('mobile');
    $isVerified=$request->input('isVerified');

    $food=DB::table('users')->where('id',$id)->first();
        
    
    DB::table('users')->where('id',$id)->update(["name"=>$name]);
    DB::table('users')->where('id',$id)->update(["email"=>$email]);
    DB::table('users')->where('id',$id)->update(["mobile"=>$mobile]);
    DB::table('users')->where('id',$id)->update(["isVerified"=>$isVerified]);
    

    if(DB::table('users')->where('id',$id)->first()){
    	$result['message']='User has been updated';
    	$result['success']="1";
    	$result['error']="0";
        return response()->json(['results'=>$result]);  
    }
    else{
    	$result['message']='Try Again, something wrong happen';
    	$result['success']="0";
    	$result['error']='1';
        return response()->json(['results'=>$result]);             
    }

    
}



public function updatebooking(Request $request)
{
    //validate data
    $validator = Validator::make($request->all(), [
        'name' => 'required',
        'address' => 'required',
        'usermobile' => 'required',
        'deliverymobile' => 'required',
        'token'=>'required',

    ]);

    if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

    if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
    {
        $result['message']='Enter valid token';
        $result['error']="1";
        $result['success']="0";
        return response()->json(['results'=>$result]);
    }


    $bookingid=$request->input('bookingid');
    $name=$request->input('name');
    $address=$request->input('address');
    $usermobile=$request->input('usermobile');
    $deliverymobile=$request->input('deliverymobile');

    $food=DB::table('bookings')->where('bookingid',$bookingid)->first();
        

    DB::table('bookings')->where('bookingid',$bookingid)->update(["name"=>$name]);
    DB::table('bookings')->where('bookingid',$bookingid)->update(["address"=>$address]);
    DB::table('bookings')->where('bookingid',$bookingid)->update(["usermobile"=>$usermobile]);
    DB::table('bookings')->where('bookingid',$bookingid)->update(["deliverymobile"=>$deliverymobile]);


    if($bookingdetails=DB::table('bookings')->where('bookingid',$bookingid)->first()){
        $result['error']='0';
    	$result['success']="1";
    	$result['message']="Order has been updated";
        return response()->json(['results'=>$result]); 
    }
    else{
        $result['message']='Try Again, something wrong happen';
        $result['error']="1";
    	$result['success']="0";
        return response()->json(['results'=>$result]);           

    }


}

//delete food
public function deletefood(Request $request)
{
    //validate data
    $validator = Validator::make($request->all(), [
        'foodid' => 'required',
        'token'=>'required',

    ]);

    if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

    if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
    {
        $result['message']='Enter valid token';
        $result['error']="1";
        $result['success']="0";
        return response()->json(['results'=>$result]);
    }

    $foodid=$request->input('foodid');
    DB::table('foods')->where('foodid',$foodid)->delete();
    if(!$fooddetails=DB::table('foods')->where('foodid',$foodid)->first()){
    	$result['error']='0';
    	$result['success']="1";
    	$result['message']="Food has been deleted";
        return response()->json(['results'=>$result]); 
    }
    else{

    	$result['message']='Try Again, something wrong happen';
    	$result['success']="0";
    	$result['error']="1";
        return response()->json(['results'=>$result]);            
    
    }
}

    //delete user
    public function deleteuser(Request $request){
    //validate data
    $validator = Validator::make($request->all(), [
        'id' => 'required',
        'token'=>'required',

    ]);

    if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

    if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
    {
       $result['message']='Enter valid token';
       $result['error']="1";
        $result['success']="0";
        return response()->json(['results'=>$result]);
    }
        $id=$request->input('id');
        DB::table('users')->where('id',$id)->delete();
        if(!DB::table('users')->where('id',$id)->first()){
        	$result['error']='0';
	    	$result['success']="1";
	    	$result['message']="User has been deleted";
	        return response()->json(['results'=>$result]); 
        }
        else{
        	$result['message']='Try Again, something wrong happen';
        	$result['error']="1";
	    	$result['success']="0";
	        return response()->json(['results'=>$result]);           
        }
    }





    public function showuserbookings(Request $request){
	    $validator = Validator::make($request->all(), [
		    'id' => 'required',
	    	'token'=>'required',

    	]);

    if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

    if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
    {
        $result['message']='Enter valid token';
        $result['error']="1";
        $result['success']="0";
        return response()->json(['results'=>$result]);
    }
    $userid=Request('id');
    // $userid=$request->input('userid');
    if($user=DB::table('users')->where('id',$userid)->first())
    {
		$mobile=$user->mobile;
	    $bookings=DB::table('bookings')->where('usermobile',$mobile)->orderBy('bookingid', 'desc')->get();
	    $result['error']='0';
        $result['success']="1";
        $result['message']="Bookings of the user:";
        $result['bookings']=$bookings;
        return response()->json(['results'=>$result]);

    }
    {
    	$result['message']='Enter a valid id';
    	$result['error']="1";
        $result['success']="0";
        return response()->json(['results'=>$result]);
    }
  }

    public function showusertiffins(Request $request){
        $validator = Validator::make($request->all(), [
        'id' => 'required',
        'token'=>'required',
        'bookingid'=>'required',

    ]);

    if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

    if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
    {
        $result['message']='Enter valid token';
        $result['success']="0";
        $result['error']="1";
        return response()->json(['results'=>$result]);
    }

    $bookingid=Request('bookingid');
    $userid=Request('id');
    $tiffins=DB::table('tiffin')->where('customerid',$userid)->where('bookingid',$bookingid)->get();
    
    $result['error']='0';
    $result['success']="1";
    $result['message']="Tifffins of the user:";
    $result['tiffins']=$tiffins;
    return response()->json(['results'=>$result]);

  }
//not used yet
    public function showtiffinson(Request $request){
        $validator = Validator::make($request->all(), [
        'date' => 'required',
        'token'=>'required',

    ]);

    if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

    if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
    {
        $result['message']='Enter valid token';
        $result['success']="0";
        $result['error']="1";
        return response()->json(['results'=>$result]);
    }
    $y = Request('date');
    $tiffins=DB::table('tiffin')->where('date',$y)->get();

    $result['error']='0';
    $result['success']="1";
    $result['message']="Tiffins on this date:";
    $result['tiffins']=$tiffins;
    return response()->json(['results'=>$result]);
}

//not used yet
    public function showfoodtiffinson(Request $request){
        $validator = Validator::make($request->all(), [
        'foodname' => 'required',
        'token'=>'required',
        'date'=>'required',

    ]);

    if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

    if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
    {	$result['message']='Enter valid token';
        $result['success']="0";
        $result['error']="1";
        return response()->json(['results'=>$result]);
    }
    $foodname=Request('foodname');
    $date=Request('date');
    $tiffins=DB::table('tiffin')->where('date',$date)->where('typemeal',"Veg Value Meal")->get();
    
    $result['error']='0';
	$result['success']="1";
	$result['message']="Tifffins on the date of type:";
	$result['tiffins']=$tiffins;
    return response()->json(['results'=>$result]);
}
        


//create user
public function createuser(Request $request)
{
    //validate data
    $validator = Validator::make($request->all(), [
        'name' => 'required',
        'email' => 'required',
        'password' => 'required',
        'mobile'=>'required|unique:users',
        'token'=>'required',
    ]);

    if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

    if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
    {
        $result['message']='Enter valid token';
        $result['success']="0";
        $result['error']="1";
        return response()->json(['results'=>$result]);
    }

    $input = $request->all();
    $input['password'] = bcrypt($input['password']);
    $input['isVerified']=1;

    //create user 
    $user = User::create($input);


    
    $mobile= $user->mobile;
    if($userdetails=DB::table('users')->where('mobile',$mobile)->first()){
    	    $result['error']='0';
		    $result['success']="1";
		    $result['message']="User has been created";
    		return response()->json(['results'=>$result]);  
    }
    else{
    	    $result['message']='Try Again,Something wrong happen';
		    $result['success']="0";
		    $result['error']="1";
            return response()->json(['results'=>$result]);    
    }
}

    public function mealsperday(Request $request){
        $validator = Validator::make($request->all(), [
            'token'=>'required',
            'date'=>'required',
        ]);

        if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

        if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
        {
            $result['message']='Enter valid token';
	        $result['success']="0";
	        $result['error']="1";
	        return response()->json(['results'=>$result]);
        }

        $date=Request('date');
        $L=Request('L');
        $D=Request('D');
        if($date==""||($L==""&&$D==""))
           {
            $result['message']="check parameters";
            $result['error']="1";
            $result['success']="0";
            return response()->json(['results'=>$result]);
        }
        
         $meals=DB::table('foods')->get();
         $i=0;
         foreach($meals as $meal){
            $data["name"]=$meal->name;
            $data["foodid"]=$meal->foodid;
            if($L==1){
                $count=DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$meal->foodid,'L'=>$L])->get();
                $data['sufficientcount']=$count->where('payment_status',"sufficient")->count();
                $data['insufficientcount']=$count->where('payment_status',"insufficient")->count();
                $data['creditcount']=$count->where('payment_status',"credit")->count();
                $data['cancelledcount']=$count->where('payment_status',"cancelled")->count();
                $data['paidcount']=$count->where('payment_status',"paid")->count();

                if($tiffin=$count->first()){
                    $data["status"]=$tiffin->status;
                }
                else{
                    $data["status"]="0";
                }
            }

            if($D==1){
                $count=DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$meal->foodid,'D'=>$D])->get();
                $data['sufficientcount']=$count->where('payment_status',"sufficient")->count();
                $data['insufficientcount']=$count->where('payment_status',"insufficient")->count();
                $data['creditcount']=$count->where('payment_status',"credit")->count();
                $data['cancelledcount']=$count->where('payment_status',"cancelled")->count();
                $data['paidcount']=$count->where('payment_status',"paid")->count();
                if($tiffin=$count->first()){
                    $data["status"]=$tiffin->status;
                }
                else{
                    $data["status"]="0";
                }
            }
            
                $d[$i++]=$data;
           } 
            return response()->json(['results'=>$d]);
    }


    //change status
    public function changestatus(Request $request){
        //validate data
        $validator = Validator::make($request->all(), [
            'token'=>'required',
            'selected'=>'required',
            'mealtime'=>'required',
            'date'=>'required',
            'foodid'=>'required',
        ]);

        if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

        if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
        {
            $result['message']='Enter valid token';
	        $result['success']="0";
	        $result['error']="1";
	        return response()->json(['results'=>$result]);
        }
        $foodid=Request('foodid');
        $date=Request('date');
        $mealtime=Request('mealtime');
        $selected=Request('selected');
      
        //return response()->json(['results'=>$selected]);


        if($mealtime=="L"){

            if(($insuff=DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$foodid,'L'=>'1','payment_status'=>'insufficient'])->get()->count())!=0)
                {
                    $result['test']=$insuff;
                    $result['error']='1';
                    $result['success']="0";
                    $result['message']="Resolve the insuffient tiffins first";
                    return response()->json(['results'=>$result]);
                }

            $sufftiffins=DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$foodid,'L'=>'1','payment_status'=>'sufficient'])->get();
            if($sufftiffins->count()!=0)
                {
                    if($selected=="packed"){
                        DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$foodid,'L'=>'1','payment_status'=>'sufficient'])->update(["status"=>'packed']);

                        foreach ($sufftiffins as $sufftiffin){
                        $customerid=$sufftiffin->customerid;
                        $user=DB::table('users')->where('id',$customerid)->first();
                        $newbalance=$user->wallet-$sufftiffin->charges;
                        DB::table('users')->where('id',$customerid)->update(['wallet'=>$newbalance]);
                        DB::table('tiffin')->where('tiffinid',$sufftiffin->tiffinid)->update(['payment_status'=>'paid']);
                        $result['try']=$this->updatepaymentstatusbulk($date,$mealtime,$customerid);
                        }
                    }
                }

            $paidtiffins=DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$foodid,'L'=>'1','payment_status'=>'paid'])->get();

            if($paidtiffins->count()!=0)
                {
                    if($selected=="delivered"){
                        DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$foodid,'L'=>'1','payment_status'=>'paid'])->update(["status"=>'delivered']);
                    }
                }

            if(DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$foodid,'L'=>'1','payment_status'=>'credit'])->get()->count()!=0)
                {
                    DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$foodid,'L'=>'1','payment_status'=>'credit'])->update(["status"=>$selected]);
                }



            	$result['error']='0';
	            $result['success']="1";
	            $result['message']="Lunch status has been updated";
	            return response()->json(['results'=>$result]);
        }

        if($mealtime=="D"){

            if(($insuff=DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$foodid,'D'=>'1','payment_status'=>'insufficient'])->get()->count())!=0)
                {
                    $result['test']=$insuff;
                    $result['error']='1';
                    $result['success']="0";
                    $result['message']="Resolve the insuffient tiffins first";
                    return response()->json(['results'=>$result]);
                }

            $sufftiffins=DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$foodid,'D'=>'1','payment_status'=>'sufficient'])->get();
            if($sufftiffins->count()!=0)
                {
                    if($selected=="packed"){
                        DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$foodid,'D'=>'1','payment_status'=>'sufficient'])->update(["status"=>'packed']);

                        foreach ($sufftiffins as $sufftiffin){
                            $customerid=$sufftiffin->customerid;
                            $user=DB::table('users')->where('id',$customerid)->first();
                            $newbalance=$user->wallet-$sufftiffin->charges;
                            DB::table('users')->where('id',$customerid)->update(['wallet'=>$newbalance]);
                            DB::table('tiffin')->where('tiffinid',$sufftiffin->tiffinid)->update(['payment_status'=>'paid']);
                            $result['try']=$this->updatepaymentstatusbulk($date,$mealtime,$customerid);

                        }
                    }
                }

            $paidtiffins=DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$foodid,'D'=>'1','payment_status'=>'paid'])->get();

            if($paidtiffins->count()!=0)
                {
                    if($selected=="delivered"){
                        DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$foodid,'D'=>'1','payment_status'=>'paid'])->update(["status"=>'delivered']);
                    }
                }

            if(DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$foodid,'D'=>'1','payment_status'=>'credit'])->get()->count()!=0)
                {
                    DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$foodid,'D'=>'1','payment_status'=>'credit'])->update(["status"=>$selected]);
                }



                $result['error']='0';
                $result['success']="1";
                $result['message']="Dinner status has been updated";
                return response()->json(['results'=>$result]);
        }
    }

    public function updatepaymentstatusbulk($date,$mealtime,$customerid)
    {
        // $customerid=Request('customerid');
        // $date=Request('date');
        // $mealtime=Request('mealtime');

        $wallet=DB::table('users')->where('id',$customerid)->first()->wallet;

        if($mealtime=="L")
        {
            $dinnertiffins=DB::table('tiffin')->where(['date'=>$date,'D'=>'1','customerid'=>$customerid])->orderBy('charges','asc')->get();

            if($dinnertiffins->count()!=0){
                foreach ($dinnertiffins as $dinnertiffin){
                    $s=$dinnertiffin->payment_status;
                    
                    if(($s!="cancelled")&&($s!="paid")&&($s!="credit")){
                         if($wallet>=$dinnertiffin->charges)
                        {
                            DB::table('tiffin')->where('tiffinid',$dinnertiffin->tiffinid)->update(['payment_status'=>'sufficient']);
                            $wallet=$wallet-$dinnertiffin->charges;
                        }
                        else{
                            DB::table('tiffin')->where('tiffinid',$dinnertiffin->tiffinid)->update(['payment_status'=>'insufficient']);
                        }
                    } 
                }
            }
            

            $nextdate=date ("Y-m-d", strtotime("+1 day", strtotime($date)));
            $lastdate=DB::table('tiffin')->where('customerid',$customerid)->whereDate('date','>',$date)->orderBy('date','desc')->first()->date;

            $p=0;
            while($nextdate<=$lastdate) {

                $lunchtiffins=DB::table('tiffin')->where(['customerid'=>$customerid,'date'=>$nextdate])->where('L','1')->orderBy('charges','asc')->get();

                if($lunchtiffins->count()!=0){

                    foreach ($lunchtiffins as $lunchtiffin){
                        $s=$lunchtiffin->payment_status;
                        
                        if(($s!="cancelled")&&($s!="paid")&&($s!="credit")){
                             if($wallet>=$lunchtiffin->charges)
                            {
                                DB::table('tiffin')->where('tiffinid',$lunchtiffin->tiffinid)->update(['payment_status'=>'sufficient']);
                                $wallet=$wallet-$lunchtiffin->charges;
                            }
                            else{
                                DB::table('tiffin')->where('tiffinid',$lunchtiffin->tiffinid)->update(['payment_status'=>'insufficient']);
                            }
                        } 
                    }
                }

                $dinnertiffins=DB::table('tiffin')->where(['customerid'=>$customerid,'date'=>$nextdate])->where('D','1')->orderBy('charges','asc')->get();

                if($dinnertiffins->count()!=0){

                    foreach ($dinnertiffins as $dinnertiffin){
                        $s=$dinnertiffin->payment_status;
                        
                        if(($s!="cancelled")&&($s!="paid")&&($s!="credit")){
                             if($wallet>=$dinnertiffin->charges)
                            {
                                DB::table('tiffin')->where('tiffinid',$dinnertiffin->tiffinid)->update(['payment_status'=>'sufficient']);
                                $wallet=$wallet-$dinnertiffin->charges;
                            }
                            else{
                                DB::table('tiffin')->where('tiffinid',$dinnertiffin->tiffinid)->update(['payment_status'=>'insufficient']);
                            }
                        } 
                    }
                }
                
                $nextdate = date ("Y-m-d", strtotime("+1 day", strtotime($nextdate)));
    
                }
        }

        if($mealtime=="D")
        {
            
            $nextdate=date ("Y-m-d", strtotime("+1 day", strtotime($date)));
            $lastdate=DB::table('tiffin')->where('customerid',$customerid)->whereDate('date','>',$date)->orderBy('date','desc')->first()->date;

            $p=0;
            while($nextdate<=$lastdate) {

                $lunchtiffins=DB::table('tiffin')->where(['customerid'=>$customerid,'date'=>$nextdate])->where('L','1')->orderBy('charges','asc')->get();

                if($lunchtiffins->count()!=0){

                    foreach ($lunchtiffins as $lunchtiffin){
                        $s=$lunchtiffin->payment_status;
                        
                        if(($s!="cancelled")&&($s!="paid")&&($s!="credit")){
                             if($wallet>=$lunchtiffin->charges)
                            {
                                DB::table('tiffin')->where('tiffinid',$lunchtiffin->tiffinid)->update(['payment_status'=>'sufficient']);
                                $wallet=$wallet-$lunchtiffin->charges;
                            }
                            else{
                                DB::table('tiffin')->where('tiffinid',$lunchtiffin->tiffinid)->update(['payment_status'=>'insufficient']);
                            }
                        } 
                    }
                }

                $dinnertiffins=DB::table('tiffin')->where(['customerid'=>$customerid,'date'=>$nextdate])->where('D','1')->orderBy('charges','asc')->get();

                if($dinnertiffins->count()!=0){

                    foreach ($dinnertiffins as $dinnertiffin){
                        $s=$dinnertiffin->payment_status;
                        
                        if(($s!="cancelled")&&($s!="paid")&&($s!="credit")){
                             if($wallet>=$dinnertiffin->charges)
                            {
                                DB::table('tiffin')->where('tiffinid',$dinnertiffin->tiffinid)->update(['payment_status'=>'sufficient']);
                                $wallet=$wallet-$dinnertiffin->charges;
                            }
                            else{
                                DB::table('tiffin')->where('tiffinid',$dinnertiffin->tiffinid)->update(['payment_status'=>'insufficient']);
                            }
                        } 
                    }
                }
                
                $nextdate = date ("Y-m-d", strtotime("+1 day", strtotime($nextdate)));
    
                }
        }


        $result['tiffin']="success";
        return response()->json(['results'=>$result]);
        
    }


    public function updatepaymentstatussingle(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token'=>'required',
            'selected'=>'required',
            'tiffinid'=>'required',
        ]);

        if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }

        if(!DB::table('adminaccesstoken')->where('token',Request('token'))->first())
        {
            $result['message']='Enter valid token';
	        $result['success']="0";
	        $result['error']="1";
	        return response()->json(['results'=>$result]);
        }
        $tiffinid=Request('tiffinid');
        $selected=Request('selected');

            if(DB::table('tiffin')->where(['tiffinid'=>$tiffinid])->get()->count()!=0)
                {
                    DB::table('tiffin')->where(['tiffinid'=>$tiffinid])->update(["payment_status"=>$selected]);
                }



            	$result['error']='0';
                $result['success']="1";
                $result['changetiffinid']=$tiffinid;
                $result['changepaymentstatus']=$selected;
	            $result['message']="Tiffin payment status has been updated";
	            return response()->json(['results'=>$result]);
        }

    

     //create booking
    public function  admincreatebooking(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'token'=>'required',
            'L' => 'required',
            'D' => 'required',
            'name' => 'required',
            'address' => 'required',
            'usermobile' => 'required',
            'deliverymobile' => 'required',
            'bookingoption' => 'required',
            'typemeal' => 'required',
            'starting_date'=>'required',
            'no_of_subscriptions'=>'required',
        ]);


        if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
        }
        
        $input = $request->all();
        $token=$request->input('token');

        $adminaccess=DB::table('adminaccesstoken')->where('token',$token)->first();
       if(!$adminaccess)
        {
            $result['message']='Enter valid token';
	        $result['success']="0";
	        $result['error']="1";
	        return response()->json(['results'=>$result]);
        }

        if(!DB::table('foods')->where('foodid',$input['typemeal'])->first())
            {
                $result['message']='Enter correct meal';
                $result['error']="1";
                $result['success']='0';
                return response()->json(['results'=>$result]);
            }
        if(!DB::table('users')->where('mobile',$input['usermobile'])->first())
            {
                $result['message']='Enter correct user mobile';
                $result['success']='0';
                $result['error']="1";
                return response()->json(['results'=>$result]);
            }

        //create booking 
        $createbooking = booking::create($input);
        //getting user details
        $booking=DB::table('bookings')->where('usermobile',$createbooking->usermobile)->orderBy('created_at','desc')->first();
        $bookingid=$booking->bookingid;
        $usermobile=$createbooking->usermobile;
        $bookingoption=$createbooking->bookingoption;
        $typemeal=$request->input('typemeal');
        $user=DB::table('users')->where('mobile',$usermobile)->first();

        $curr_date=$createbooking->starting_date;

        if($createbooking->bookingoption=="weekly"){
            $lastdate=strtotime("+6 days",strtotime($curr_date));
            $lastdate=date('Y-m-d',$lastdate);
            DB::table('bookings')->where('bookingid',$bookingid)->update(["last_date"=>$lastdate]);
        }

        if($createbooking->bookingoption=="monthly"){
            $lastdate=strtotime("+29 days",strtotime($curr_date));
            $lastdate=date('Y-m-d',$lastdate);
            DB::table('bookings')->where('bookingid',$bookingid)->update(["last_date"=>$lastdate]);
        }

        $charges=DB::table('foods')->where('foodid',$typemeal)->first()->dailyprice;
        $quantity=$createbooking->no_of_subscriptions;


        $curr_date=$createbooking->starting_date;
        //weekly
        if($createbooking->bookingoption=="weekly")
        {
            
                for($i=0;$i<7*$quantity;$i++){

                    if($createbooking->L){
                         DB::table('tiffin')->insert(array(
                            array("L"=>"$createbooking->L",
                              "D"=>"0",
                              "bookingid"=>"$bookingid",
                              "customerid"=>"$user->id",
                              "bookingoption"=>"$createbooking->bookingoption",
                              "date"=>"$curr_date",
                              "status"=>"pending",
                              "typemeal"=>"$typemeal",
                              "charges"=>$charges
                              )
                            )
                         );
                    }
                   
                    if($createbooking->D){
                            DB::table('tiffin')->insert(array(
                            array("L"=>"0",
                              "D"=>"$createbooking->D",
                              "bookingid"=>"$bookingid",
                              "customerid"=>"$user->id",
                              "bookingoption"=>"$createbooking->bookingoption",
                              "date"=>"$curr_date",
                              "status"=>"pending",
                              "typemeal"=>"$typemeal",
                              "charges"=>$charges
                              )
                            )
                           );
                        }
                    
                    $curr_date=date('Y-m-d', strtotime('+1 day', strtotime($curr_date)));
                 }
        }

        $curr_date=$createbooking->starting_date;
        //monthly
        if($createbooking->bookingoption=="monthly")
            {
        
            for($i=0;$i<30*$quantity;$i++){

                if($createbooking->L){
                     DB::table('tiffin')->insert(array(
                        array("L"=>"$createbooking->L",
                          "D"=>"0",
                          "bookingid"=>"$bookingid",
                          "customerid"=>"$user->id",
                          "bookingoption"=>"$createbooking->bookingoption",
                          "date"=>"$curr_date",
                          "status"=>"pending",
                          "typemeal"=>"$typemeal",
                          "charges"=>$charges
                          )
                        )
                     );
                }
               
                if($createbooking->D){
                        DB::table('tiffin')->insert(array(
                        array("L"=>"0",
                          "D"=>"$createbooking->D",
                          "bookingid"=>"$bookingid",
                          "customerid"=>"$user->id",
                          "bookingoption"=>"$createbooking->bookingoption",
                          "date"=>"$curr_date",
                          "status"=>"pending",
                          "typemeal"=>"$typemeal",
                          "charges"=>$charges
                          )
                        )
                       );
                    }
                
                $curr_date=date('Y-m-d', strtotime('+1 day', strtotime($curr_date)));
             }
        }

        if($orderdetails=DB::table('bookings')->where('bookingid',$bookingid)->first()){
            $this->updatepaymentstatusbooking($usermobile,$bookingid);
            // $result['test']=$test;

            $result['message']='Order has been created';
            $result['success']='1';
            $result['error']='0';
            return response()->json(['results'=>$result]);
        }
        else{
            $result['message']='Unable to create order';
            $result['success']='0';
            $result['error']="1";
            return response()->json(['results'=>$result]);        
        }

       }

       public function updatepaymentstatusbooking($usermobile,$bookingid)
        {
            $user=DB::table('users')->where('mobile',$usermobile)->first();
            $userwallet=$user->wallet;
            $tiffins=DB::table('tiffin')->where('bookingid',$bookingid)->orderBy('tiffinid', 'asc')->get();
            $tiffinscount=$tiffins->count();
            $charge=$tiffins->first()->charges;

            
            $n =floor($userwallet/$charge);
            // return response()->json(['results'=>$n]); 

            for($i=0;$i<$tiffinscount;$i++)
            {
                if($i<$n){
                    $tiffinid=$tiffins[$i]->tiffinid;
                    DB::table('tiffin')->where('tiffinid',$tiffinid)->update(['payment_status'=>'sufficient']);
                }
                else{
                    $tiffinid=$tiffins[$i]->tiffinid;
                    DB::table('tiffin')->where('tiffinid',$tiffinid)->update(['payment_status'=>'insufficient']);
                }
                
            }

       }


        //updatetiffins
        public function adminupdatetiffin(Request $request)
        {
             //validate data
            $validator = Validator::make($request->all(), [
                'token'=>'required',
                'bookingid'=>'required',
                'tiffinid'=>'required',
                'date'=>'required',
                'typemeal'=>'required',
            ]);

            if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
            }

            $token=$request->input('token');
            $bookingid=$request->input('bookingid');
            $tiffinid=$request->input('tiffinid');
            $updatedate=$request->input('date');
            $updatemeal=$request->input('typemeal');
           //check access
           
           $adminaccess=DB::table('adminaccesstoken')->where('token',$token)->first();
           if(!$adminaccess)
            {
                $result['message']='Enter valid token';
		        $result['success']="0";
		        $result['error']="1";
		        return response()->json(['results'=>$result]);
            }

             if(!DB::table('foods')->where('foodid',$updatemeal)->first())
            {
                $result['message']='Enter correct meal';
		        $result['success']="0";
		        $result['error']="1";
		        return response()->json(['results'=>$result]);
            }
            
            $tiffin=DB::table('tiffin')->where('bookingid',$bookingid)->where('tiffinid',$tiffinid)->first();
            $forlastdate=DB::table('bookings')->where('bookingid',$bookingid)->first();

            $currentdate=Carbon\Carbon::now()->toDateString();
            $lastdate=$forlastdate->last_date;
            $lastdatenew=strtotime("+3 days",strtotime($lastdate));
            $lastdatenew=date('Y-m-d',$lastdatenew);
            
        //update date
            if($updatedate>=$currentdate && $updatedate<=$lastdatenew){
                DB::table('tiffin')->where('bookingid',$bookingid)->where('tiffinid',$tiffinid)->update(["date"=>$updatedate]);
                }       
            
            
            else{
                $result['message']="Enter a valid date between ". $currentdate." and ".$lastdatenew;
                $result['success']="0";
                $result['error']="1";
                return response()->json(['results'=>$result]);
            }
            
            
        $food=DB::table('foods')->where('foodid',$updatemeal)->first();
        $t=DB::table('tiffin')->where('bookingid',$bookingid)->where('tiffinid',$tiffinid);
        
        if($t->first()){
            $t->update(["typemeal"=>$updatemeal]);
            $t->update(["charges"=>$food->dailyprice]);
            $result['success']="1";
            $result['error']="0";
            $result['message']="Tiffin has been updated";
            return response()->json(['results'=>$result]);  
        }
        else{
            $result['success']="0";
            $result['error']="1";
            $result['message']="Try again, something went wrong";
            return response()->json(['results'=>$result]);          
        
        }
    }

      //viewadmintiffins
        public function adminviewtiffins(Request $request)
        {
             //validate data
            $validator = Validator::make($request->all(), [
                'token'=>'required',
                'bookingid'=>'required',
            ]);

            if ($validator->fails()) {
            $msg['success']='0';
            $msg['error']=$validator->errors();
            $msg['message']="validator error";
            return response()->json(['results' => $msg]);           
            }

            $token=Request('token');
            $bookingid = Request('bookingid');
           //check access
           
           $adminaccess=DB::table('adminaccesstoken')->where('token',$token)->first();
           if(!$adminaccess)
            {
                $result['message']='Enter valid token';
		        $result['success']="0";
		        $result['error']="1";
		        return response()->json(['results'=>$result]);
            }

            $viewtiffins=DB::table('tiffin')->where('bookingid',$bookingid)->get();
            $result['success']="1";
            $result['error']="0";
            $result['tiffins']=$viewtiffins;
            $result['message']="Tiffins of the particular booking";
            return response()->json(['results'=>$result]);
        }

    public function checkstatus(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'token'=>'required',
            'typemeal'=>'required',
            'date'=>'required',
            'mealtime'=>'required',
            'paymentstatus'=>'required'
        ]);

        if ($validator->fails()) {
        $msg['success']='0';
        $msg['error']=$validator->errors();
        $msg['message']="validator error";
        return response()->json(['results' => $msg]);           
        }

        $token=$request->input('token');
       //check access
       
       $adminaccess=DB::table('adminaccesstoken')->where('token',$token)->first();
       if(!$adminaccess)
        {
            $result['message']='Enter valid token';
            $result['success']="0";
            $result['error']="1";
            return response()->json(['results'=>$result]);
        }

        $date=$request->input('date');
        $typemeal=$request->input('typemeal');
        $fortypemealname=DB::table('foods')->where(['foodid'=>$typemeal])->first();
        $typemealname=$fortypemealname->name;
        $paymentstatus=$request->input('paymentstatus');
        $mealtime=$request->input('mealtime');

        if($paymentstatus==1)$paymentstatus="sufficient";
        if($paymentstatus==2)$paymentstatus="insufficient";
        if($paymentstatus==3)$paymentstatus="credit";
        if($paymentstatus==4)$paymentstatus="cancelled";
        if($paymentstatus==5)$paymentstatus="paid";

        if($mealtime=="L"){
            $t=DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$typemeal,'payment_status'=>$paymentstatus,'L'=>"1"]);
            $tiffin=$t->get();
            $tiffincount=$t->get()->count();
        }

        if($mealtime=="D"){
            $t=DB::table('tiffin')->where(['date'=>$date,'typemeal'=>$typemeal,'payment_status'=>$paymentstatus,'D'=>"1"]);
            $tiffin=$t->get();
            $tiffincount=$t->get()->count();
        }
        
        if($tiffincount==0)
        {
            $result['error']=0;
            $result['success']=1;
            $result['message']="No Tiffins found";
            $result['paymentstatus']=$paymentstatus;
            $result['tiffincount']=$tiffincount;
            return response()->json(['results'=>$result]);
        }

        for ($i=0; $i <$tiffincount; $i++) { 
            $check['bookingid']=$tiffin[$i]->bookingid;
            $check['tiffinid']=$tiffin[$i]->tiffinid;
            $booking=DB::table('bookings')->where('bookingid',$check['bookingid'])->first();
            $check['name']=$booking->name;
            $check['address']=$booking->address;
            $check['usermobile']=$booking->usermobile;
            $check['deliverymobile']=$booking->deliverymobile;
            $check['typemeal']=$typemealname;
            $check['status']=$tiffin[$i]->status;
            $d[$i]=$check;
        }
        $result['tiffincount']=$tiffincount;
        $result['paymentstatus']=$paymentstatus;
        $result['tiffins']=$d;
        $result['error']=0;
        $result['success']=1;
        $result['message']="check the tiffins";
        return response()->json(['results'=>$result]);
    }
}
