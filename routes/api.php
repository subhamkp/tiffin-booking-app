<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/updatepaymentstatuschange', 'admincontroller@updatepaymentstatuschange');



//APP INFO
Route::get('/info', 'appcontroller@info');

//Registration and verification
Route::post('/verifyotp', 'msg@verifyaccount');
Route::post('/register','usercontroller@register');
Route::post('/login','usercontroller@login');
Route::post('/logout','usercontroller@logout');
Route::post('/resendotp','msg@getotp');

Route::post('/password/forget', 'msg@getotp');
Route::post('/password/reset', 'usercontroller@resetpassword');

//FOOD
Route::post('/food', 'foodcontroller@food');
Route::post('/breakfast','foodcontroller@breakfast');
Route::post('/lunch','foodcontroller@lunch');
Route::post('/dinner','foodcontroller@dinner');
//create booking
Route::post('/createbooking','foodcontroller@createbooking');
//viewbooking
Route::post('/viewbooking','foodcontroller@viewbooking');
//viewtiffins
Route::post('/viewtiffins','foodcontroller@viewtiffins');
//updatetiffin
Route::post('/updatetiffin','foodcontroller@updatetiffin');

//admin side

//create food
Route::post('/createfood','admincontroller@createfood');
//create booking
Route::post('/admincreatebooking','admincontroller@admincreatebooking');
Route::post('/adminupdatetiffin','admincontroller@adminupdatetiffin');
Route::post('/adminviewtiffins','admincontroller@adminviewtiffins');
//update foof
Route::post('/updatefood','admincontroller@updatefood');
//delete food
Route::post('/deletefood','admincontroller@deletefood');
//delete user
Route::post('/deleteuser','admincontroller@deleteuser');
Route::post('/updateuser','admincontroller@updateuser');
//show booking
Route::post('/showuserbookings','admincontroller@showuserbookings');
//show tiffins
Route::post('/showusertiffins','admincontroller@showusertiffins');
//tiffins on a date
Route::post('/showtiffinson','admincontroller@showtiffinson');
Route::post('/showbookings','admincontroller@showbookings');
Route::post('/showusers','admincontroller@showusers');
Route::post('/showtiffins','admincontroller@showtiffins');
Route::post('/checkstatus','admincontroller@checkstatus');


Route::post('/createuser','admincontroller@createuser');

Route::post('/verifyadmin','admincontroller@verifyadmin');
Route::post('/adminlogout','admincontroller@adminlogout');
Route::post('/updatebooking','admincontroller@updatebooking');
Route::post('/mealsperday','admincontroller@mealsperday');
Route::post('/showuser','admincontroller@showuser');
Route::post('/changestatus','admincontroller@changestatus');
Route::post('/updatepaymentstatussingle','admincontroller@updatepaymentstatussingle');
Route::post('/userdetails','usercontroller@userdetails');