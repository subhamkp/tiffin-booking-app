<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){
    return view('home');
});


Route::get('/admin',function(){
    return view('login');
});

Route::get('/admin/home', function () {
    return view('welcome');
});


//orders

Route::get('/admin/orders', function () {
    return view('orders');
});

Route::get('/admin/order/create',function(){
    return view('createorder');
});

Route::get('/admin/order/view/{index}', function ($index) {
    return view('showorder')->with('bookingid',$index);
});



//tiffins

Route::get('/admin/tiffins/{index}', function ($index) {
    return view('tiffins')->with('bookingid',$index);
});

Route::get('/admin/tiffin/{index}', function ($index) {
    return view('showtiffin')->with('tiffinid',$index);
});

Route::get('/admin/tiffin', function () {
    return view('showtiffin');
});



//users

Route::get('/admin/users', function () {
    return view('users');
});

Route::get('/admin/user/view/{index}', function ($index) {
    return view('showuser')->with('id',$index);
});

Route::get('/admin/user/create',function(){
    return view('createuser');
});


//foods

Route::get('/admin/foods', function () {
    return view('foods');
});

Route::get('/admin/foods/{index}', function ($index) {
    return view('showfood')->with('foodid',$index);
});

Route::get('/admin/food', function () {
    return view('showfood');
});

Route::get('/admin/food/create', function () {
    return view('createfood');
});



//bookings

Route::get('/admin/user/bookings/{index}', function ($index) {
    return view('showuserbooking')->with('id',$index);
});

Route::get('/admin/user/booking/tiffin/{index}/{indexx}', function ($index,$indexx) {
    return view('showusertiffins')->with('id',$index)->with('bookingid',$indexx);
});

//DEFINED TWICE??
// Route::get('/admin/food/create',function(){
//     return view('createfood');
// });




