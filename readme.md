Tiffin Booking App
- 

USER
-  

User can register and logIn to account by verifying the sent OTP

	Passport and MSG91

Verified User can book tiffins on weekly or Monthly basis.

User can view the food items and their prices.

ADMIN
-

Admin can update the food, their prices and add new food items.

It can change the status of the tiffins in one go

It is necessary for the admin to resolve the tiffins for users having insufficient balance. 